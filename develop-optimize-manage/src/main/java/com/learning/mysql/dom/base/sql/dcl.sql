# 第一部分 基础篇 SQL基础

### DCL 操作 ###
# DCL 主要是DBA 用来管理系统中对象权限时使用

# 创建一个用户,语法
CREATE USER 'username'@'host'
  IDENTIFIED BY 'password';

# 创建sam 用户
CREATE USER 'sam'@'localhost'
  IDENTIFIED BY '123456';
# host,不做限制
CREATE USER 'lee'@'%'
  IDENTIFIED BY '123456';

# 查看用户表
SELECT *
FROM mysql.user;

# 设置与更改用户密码
# SET PASSWORD FOR 'username'@'host' = PASSWORD('newpassword');
# SET PASSWORD = PASSWORD("newpassword");
SET PASSWORD FOR 'sam'@'localhost' = PASSWORD ('987654');
SET PASSWORD = PASSWORD ('987654');

# 删除用户
DROP USER 'username'@'host';
DROP USER 'sam'@'localhost';

# GRANT：赋予权限，语法：
GRANT PRIVILEGES ON databasename.tablename TO 'username'@'host';

# 赋予sam用户learning_mysql数据库所有表的SELECT、INSERT权限
GRANT SELECT, INSERT ON learning_mysql.* TO 'sam'@'localhost';
FLUSH PRIVILEGES;

# 创建用户、赋予权限。一步到位。原因：创建用户的语句其中也是赋权的过程
# GRANT USAGE ON *.* TO 'sam'@'localhost' IDENTIFIED BY PASSWORD '*6BB4837EB74329105EE4568DDA7DC67ED2CA2AD9'
GRANT SELECT, INSERT ON learning_mysql.* TO 'sam'@'localhost'
IDENTIFIED BY 'sam';

# 查看用户权限: mysql database
# 全局权限适用于一个给定服务器中的所有数据库。这些权限存储在mysql.user表中。GRANT ALL ON *.*和REVOKE ALL ON *.*只授予和撤销全局权限。
SELECT *
FROM mysql.user;

# 数据库权限适用于一个给定数据库中的所有目标。这些权限存储在mysql.db和mysql.host表中。GRANT ALL ON db_name.*和REVOKE ALL ON db_name.*只授予和撤销数据库权限。
SELECT *
FROM mysql.db;

# 表权限适用于一个给定表中的所有列。这些权限存储在mysql.tables_priv表中。GRANT ALL ON db_name.tbl_name和REVOKE ALL ON db_name.tbl_name只授予和撤销表权限。
SELECT *
FROM mysql.tables_priv;

# 列权限适用于一个给定表中的单一列。这些权限存储在mysql.columns_priv表中。当使用REVOKE时，您必须指定与被授权列相同的列。
SELECT *
FROM mysql.columns_priv;

# 查看用户的权限
SHOW GRANTS FOR 'sam'@'localhost';

# REVOKE：回收权限
REVOKE SELECT ON *.* FROM 'sam'@'%';
REVOKE INSERT ON learning.* FROM 'sam'@'localhost';