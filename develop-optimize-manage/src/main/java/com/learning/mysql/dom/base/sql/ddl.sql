# 第一部分 基础篇 SQL基础

### DDL 操作 ###

### 1-DATABASE(数据库) 操作 ###

# 创建 DATABASE
CREATE DATABASE IF NOT EXISTS learning_mysql;
# DATABASE 列表
SHOW DATABASES;
# DATABASE 选择
USE learning_mysql;
# 删除 DATABASE
DROP DATABASE IF EXISTS learning_mysql;
# 查看某个DATABASE下的所有表
SHOW TABLES;
SHOW TABLES FROM learning_mysql;
# 查询某个DATABASE下的表数据
SELECT *
FROM learning_mysql.user;

### mysql 元数据 DATABASE 操作 ###
# information_schema 主要存储了系统中一些数据库对象信息，如用户表信息、列信息、权限信息、字符集信息、分区信息等
# performance_schema
# mysql 存储了系统的用户权限信息
# test 测试数据库
SHOW TABLES FROM test;
SHOW TABLES FROM mysql;
SHOW TABLES FROM information_schema;
SHOW TABLES FROM performance_schema;

### 2-TABLE 表操作 ###

# 创建表
CREATE TABLE IF NOT EXISTS emp (
  ename    VARCHAR(10),
  hiredate DATE,
  sal      DECIMAL(10, 2),
  deptno   INT(2)
);

CREATE TABLE IF NOT EXISTS dept (
  deptno   VARCHAR(2),
  deptname VARCHAR(30)
);

# 查看表信息
DESC emp;
# 查看表创建信息：可以看到表的engine和charset等信息.
SHOW CREATE TABLE emp;
# \G 是使的记录按照字段竖向排列，更好的显示内容较长的记录
SHOW CREATE TABLE emp \G;

# 删除表
DROP TABLE IF EXISTS emp;

# 修改表

# 修改表emp的ename字段定义，VARCHAR(10)改为VARCHAR(20)
ALTER TABLE emp
  MODIFY ename VARCHAR(20);

# 增加表字段
ALTER TABLE emp
  ADD COLUMN age INT(3);
ALTER TABLE emp
  ADD age INT(3);

# 删除表字段
ALTER TABLE emp
  DROP COLUMN age;
ALTER TABLE emp
  DROP age;

# 修改列名称
# change 与modify 都可以修改表定义，change需要写两次列名，不方便.
# 但是change的有点是且可以修改列名称。
ALTER TABLE emp
  CHANGE ename ename2 VARCHAR(10);
ALTER TABLE emp
  CHANGE ename2 ename VARCHAR(10);

# 修改字段顺序
ALTER TABLE emp
  ADD COLUMN age INT(3)
  AFTER ename;
ALTER TABLE emp
  MODIFY COLUMN age INT(3) FIRST;
ALTER TABLE emp
  MODIFY age INT(3) AFTER sal;

# 更改表名
ALTER TABLE emp
  RENAME new_emp;
DESC new_emp;
ALTER TABLE new_emp
  RENAME emp;