# 第一部分 基础篇 SQL基础

### DML 操作 ###
USE learning_mysql;

# INSERT 插入记录
INSERT INTO emp (ename, hiredate, sal, deptno, age) VALUES ('sam', '2016-07-01', 10000, NULL, 30);
# 不指定字段，但是顺序与字段顺序一致
INSERT INTO emp VALUES ('sho', '2015-06-01', 8000, '09876', 28);
# 含可空字段、非空但有默认值、自增字段，可以不用在insert后面出现，values只写对应字段的值
# 如支队表中的 ename、hiredate、deptno字段显示插入值
INSERT INTO emp (ename, hiredate, deptno) VALUES ('tom', '2013-09-01', 8);
# 一个插入多行，values后面都好分开
INSERT INTO emp (ename, hiredate) VALUES ('jack', '2016-03-01'), ('marry', '2017-12-03');
INSERT INTO dept VALUES (1, 'tech'), (2, 'sale'), (3, 'fin');
INSERT INTO dept VALUES (9, 'tech');

# UPDATE 更新记录
UPDATE emp
SET emp.sal = 5000
WHERE emp.ename = 'tom';

# 同时更新多张表的数据
# 用在根据一个表的字段来动态更新另外一个表的字段
UPDATE emp a, dept b
SET a.sal = a.sal * b.deptno, b.deptname = a.ename
WHERE a.deptno = b.deptno;

# DELETE 删除记录
# 删除全表记录。主键继续计算。
DELETE FROM emp;
# 清空表：主键重新计算。
TRUNCATE TABLE emp;

# 删除某个条件的记录
DELETE FROM emp
WHERE deptno = 8;
# 两张表同时删
DELETE a, b FROM emp a, dept b
WHERE a.deptno = b.deptno AND a.deptno = 2;

# SELECT 查询记录
SELECT *
FROM emp;
SELECT *
FROM dept;

# 查询不重复记录 DISTINCT
SELECT DISTINCT
  ename,
  deptno,
  hiredate,
  age
FROM emp
WHERE deptno IS NOT NULL AND sal > 100;

# SELECT ORDER BY 排序
SELECT *
FROM dept
ORDER BY deptno DESC;

# 部门相同的 薪水逆序
SELECT *
FROM emp
ORDER BY deptno, sal DESC;

# SELECT LIMIT 分页
SELECT *
FROM emp
ORDER BY deptno
LIMIT 0, 2;
# 限制默认从0 开始
SELECT *
FROM emp
ORDER BY deptno
LIMIT 2;
# 从第二条数据开始，取3条记录
SELECT *
FROM emp
ORDER BY deptno
LIMIT 1, 3;

# SELECT 聚合
SELECT count(1)
FROM emp;

# GROUP BY 分组
SELECT
  deptno,
  count(1)
FROM emp
GROUP BY deptno;

# WITH ROLLUP 再汇总，对 count(1)的数据再汇总
SELECT
  deptno,
  count(1)
FROM emp
GROUP BY deptno WITH ROLLUP;

# 分组后的条件查询 HAVING
SELECT
  deptno,
  count(1)
FROM emp
GROUP BY deptno
HAVING count(1) > 1;

# 聚合
SELECT
  sum(sal),
  max(sal),
  min(sal)
FROM emp;

# JOIN 表连接
# 内连接：选出两张表中互相匹配的记录
# 外连接：选出其他不匹配的记录
SELECT *
FROM emp, dept
WHERE dept.deptno = emp.deptno;

# Join 实现
SELECT *
FROM emp
  JOIN dept ON (dept.deptno = emp.deptno);

# 外连接分为左连接、右连接
# 左连接:包含所有左边表记录
SELECT *
FROM emp
  LEFT JOIN dept ON dept.deptno = emp.deptno;
# 右连接：包含所有右边表记录
SELECT *
FROM emp
  RIGHT JOIN dept ON dept.deptno = emp.deptno;

# 子查询：另一个 SELECT 语句的结果作为一个查询的条件
# 用于子查询的关键字：in、 not in、=、exists、not exists等
SELECT *
FROM emp
WHERE deptno IN (SELECT deptno
                 FROM dept);
# 报错：子查询的结果不唯一
SELECT *
FROM emp
WHERE deptno = (SELECT deptno
                FROM dept);

# 有些子查询可以处理为表接连
# 表连接在很多情况下效率更高
SELECT *
FROM emp
WHERE deptno IN (SELECT deptno
                 FROM dept);
SELECT *
FROM emp
  JOIN dept ON (dept.deptno = emp.deptno);

# UNION 记录联合
# UNION ALL:把结果拼在一起
SELECT deptno
FROM emp
UNION ALL SELECT deptno
          FROM dept;

# UNION:在UNION ALL后的结果,进行 DISTINCT
SELECT deptno
FROM emp
UNION SELECT deptno
      FROM dept;
