# 第一部分 基础篇 SQL基础
### mysql  连接###
# mysql -uroot
# root

# 帮助的使用
# 按层次看帮助
? contents;
? DATA Types;
? INT;

# 快速查阅帮助
? SHOW;
? CREATE TABLE;

# 查询元数据信息
# information_schema 元数据 视图非实体表
USE information_schema;

# 数据库信息
SELECT *
FROM information_schema.SCHEMATA;
SHOW DATABASES ;

# 表信息
SELECT *
FROM information_schema.TABLES
WHERE TABLE_SCHEMA = 'learning_mysql';
SHOW TABLES FROM learning_mysql;
# 列信息
SELECT *
FROM information_schema.COLUMNS
WHERE TABLE_SCHEMA = 'learning_mysql' AND TABLE_NAME = 'emp';
SHOW COLUMNS FROM learning_mysql.emp;
# 索引信息
SELECT *
FROM information_schema.STATISTICS;
SHOW INDEX FROM learning_mysql.emp;
