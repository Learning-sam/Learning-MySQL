### BIT位类型 ####

# BIT(M) 存放多位二进制数。M范围为1~64。默认是1
# select 看不到结果，需要时用bin(二进制)、hex(十六进制)

CREATE TABLE t2 (
  id BIT
);

INSERT INTO t2 VALUES (1);

# 直接使用 SELECT 看不到结果。
SELECT *
FROM t2;

# 需要使用bin(二进制)、hex(十六进制)
SELECT
  bin(id),
  hex(id)
FROM t2;

# 数据插入bit类型字段时，首先转换为二进制。大于定义位数，则插入失败

# 插入数字2，二进制为'10',而id的定义为bit(1)，无法插入
INSERT INTO t2 VALUES (2);

# 查看报错
SHOW ERRORS;