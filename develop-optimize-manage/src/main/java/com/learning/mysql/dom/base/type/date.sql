###  日期时间类型 ####

# DATE : 年月日
# TIME : 时分秒
# DATETIME : 年月日时分秒

# TIMESTAMP : 默认会有当前系统时间。显示为YYYY-MM-DD HH:MM:SS。默认是字符串。想获取数字值，TIMESTAMP列添加'+0'
# YEAR : 年

DROP TABLE IF EXISTS t3;
CREATE TABLE t3 (
  d  DATE,
  t  TIME,
  dt DATETIME
);
DESC t3;
SELECT *
FROM t3;

# 使用 now() 函数插入当前日期
INSERT INTO t3 VALUES (now(), now(), now());

# TIMESTAMP 测试
DROP TABLE IF EXISTS t4;
CREATE TABLE t4 (
  ts TIMESTAMP
);
# 默认会给 ts 字段添加默认值 CURRENT_TIMESTAMP
DESC t4;
SELECT *
FROM t4;

# 插入一个空值，系统会给一个当前系统时间
INSERT INTO t4 VALUES (NULL);

# 注意：
# 增加第二个 TIMESTAMP 列,默认为0000-00-00 00:00:00，只允许一个默认CURRENT_TIMESTAMP
ALTER TABLE t4
  ADD COLUMN ts2 TIMESTAMP;

# 都会有默认值插入
INSERT INTO t4 VALUES (NULL, NULL);

# TIMESTAMP 有上下限值。而且有时区影响。推荐使用 DATETIME ，配合now() 使用

# 时间函数
SELECT
  now(),
  current_date,
  current_time,
  curdate(),
  curtime(),
  current_timestamp
FROM dual;

# 时间格式
DROP TABLE IF EXISTS t4;
CREATE TABLE t5 (
  dt DATETIME
);

# 全部可以正确插入
INSERT INTO t5 VALUES ('2018-05-07 20:37:44');
INSERT INTO t5 VALUES ('2018/05/07 20+37+44');
INSERT INTO t5 VALUES ('20180507203744');
INSERT INTO t5 VALUES (20180507203744);

