#### 小数类型 ####
# 浮点数、定点数
# FLOAT DOUBLE、 DECIMAL（字符串形式存放、更加精确）


# 浮点数 FLOAT DOUBLE
# (M,D) 的方式表示，M-精度、D-标度。例如float(7,4)，可以显示为-999.9999
# mysql 在保存值的时候会四舍五入，如float(7,4)列内插入999.00009,近似结果为999.0001
# 浮点数后面跟(M,D) 的用法是非标准用法，如果用于数据库迁移，最好不要使用
# FLOAT DOUBLE 不制定精度，默认按照实际精度(硬件与操作系统决定)来显示
# DECIMAL 不指定精度，默认整数位10，小数位0

# 示例
DROP TABLE IF EXISTS t1;
CREATE TABLE IF NOT EXISTS t1 (
  id1 FLOAT(5, 2)   DEFAULT NULL,
  id2 DOUBLE(5, 2)  DEFAULT NULL,
  id3 DECIMAL(5, 2) DEFAULT NULL
);

# 插入数据 1.23 ，都准确插入
INSERT INTO t1 VALUES (1.23, 1.23, 1.23);

# id1、id2 插入 1.234，id3 插入 1.23。id1、id2由于标度限制，社区了最后一位
INSERT INTO t1 VALUES (1.234, 1.234, 1.23);

# 插入数据 1.234。id3的也被截取一位。
# 系统出现一个Warning： Data truncated for column 'id3' at row 1
# 这和SQLMode先关，如果是传统的SQLMode，这条记录是没法插入的
INSERT INTO t1 VALUES (1.234, 1.234, 1.234);

# 查看警告
SHOW WARNINGS;

# 示例2:去掉表的精度与标度
ALTER TABLE t1
  MODIFY COLUMN id1 FLOAT;
ALTER TABLE t1
  MODIFY COLUMN id2 DOUBLE;
ALTER TABLE t1
  MODIFY COLUMN id3 DECIMAL;

# 插入数据 1.23。id1、id2正常插入，id3字段小数位被截断，因为默认是（10,0）
INSERT INTO t1 VALUES (1.23, 1.23, 1.23);

SELECT *
FROM t1;
