#### 整数类型 ####

# TINYINT SMALLINT MEDIUMINT INT(INTEGER) BIGINT


# 整数的宽度:默认是INT(11),未满会填满
# 一般和 zerofill 配合使用，‘0’填充


# 例子：显示填充以后的区别
CREATE TABLE IF NOT EXISTS t1 (
  id1 INT,
  id2 INT(5)
);

# 插入数据
INSERT INTO t1 VALUES (1, 1);

# 分别修改两个字段，加入 zerofill
ALTER TABLE t1
  MODIFY id1 INT ZEROFILL;
ALTER TABLE t1
  MODIFY id2 INT(5) ZEROFILL;

# 需要使用mysql 客户端才能显示
SELECT *
FROM t1;

# 设置宽度后，插入大于宽度的值不会有任何影响
# 例子： id2 插入宽度7，可以正常插入
INSERT INTO t1 VALUES (1, 1111111);

# 属性


# 属性1：UNSIGNED(无符号) , 如果需要在字段保存非负数，或者较大的上限值，可以选用。
# 如 TINYINT 有符号范围-128~+127 ，而UNSIGNED(无符号)范围是0~255。
# 如果一个列指定了 ZEROFILL ，默认还是 UNSIGNED(无符号) ，可以通过查看建表语句查看
CREATE TABLE IF NOT EXISTS t1 (
  id1 INT UNSIGNED,
  id2 INT(5) ZEROFILL
);

SHOW CREATE TABLE t1;
DESC t1;

# 属性2：AUTO_INCREMENT 自增长。只有一列允许，且定义为 NOT NULL,并且定义为 PRIMARY KEY 或者 UNIQUE
CREATE TABLE IF NOT EXISTS t1 (
  id1 INT AUTO_INCREMENT NOT NULL PRIMARY KEY UNIQUE,
  id2 INT(5)
);

CREATE TABLE IF NOT EXISTS t1 (
  id1 INT AUTO_INCREMENT NOT NULL,
  id2 INT(5),
  PRIMARY KEY (id1)
);

CREATE TABLE IF NOT EXISTS t1 (
  id1 INT AUTO_INCREMENT NOT NULL,
  id2 INT(5),
  UNIQUE (id1)
);

