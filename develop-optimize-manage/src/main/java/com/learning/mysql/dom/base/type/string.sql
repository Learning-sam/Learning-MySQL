###  字符类型 ###

# 多种字符串的存储类型：CHAR VARCHAR BINARY VARBINARY BLOB TEXT ENUM SET

# CHAR VARCHAR ： 保存较短的字符串
# CHAR(M) : 0~255。不可变。
# VARCHAR(M) : 0~65535 , 值的长度 +1 个字节。可变长字符串

# 例子：CHAR 列会删除尾部的空格，VARCHAR 保留这些空格
CREATE TABLE vc (
  v VARCHAR(4),
  c CHAR(4)
);

# 插入数据 ab空格空格
INSERT INTO vc (v, c) VALUES ('ab  ', 'ab  ');

# 发现，c的长度是2，v的长度为4
SELECT
  length(v),
  length(c)
FROM vc;

# 这样看更明显
SELECT
  concat(v, '+'),
  concat(c, '+')
FROM vc;

# BINARY VARBINARY ： 包含二进制字符串、不包含非二进制字符串

# ENUM 枚举类型,范围从1开始。一次选取一个成员
CREATE TABLE t_enum (
  gender ENUM ('M', 'F')
);

# 插入数据：大小写可以忽略、还可以插入范围值1
INSERT INTO t_enum VALUES ('M'), ('f'), ('1'), (NULL);

SELECT *
FROM t_enum;

# SET 类型 包含0~64个成员。一次选取多个成员
CREATE TABLE t_set (
  col SET ('a', 'b', 'c', 'd')
);

# ('a,a') 只会插入a,会去重
INSERT INTO t_set VALUES ('a,b'), ('a,a'), ('a');

SELECT *
FROM t_set;

