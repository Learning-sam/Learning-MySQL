# 字符集的选择
# 双字节定长中文字符集：性能上GBK(中文占2个字符)比UTF8(中文占3个字符)要好
# 西文：GBK、UTF16 西文都是2个字符，而UTF8为1个字符

# mysql 支持的字符集,和该字符集默认的校对规则
SHOW CHARACTER SET;
SELECT *
FROM information_schema.CHARACTER_SETS;

# mysql的字符集包括字符集 与 校对规则
# 字符集（CHARACTER,存储字符串的方式，30多种）与校对规则（COLLATION,比较字符串的方式,70多种）
# 查看校对集信息
SHOW COLLATION;
SELECT *
FROM information_schema.COLLATIONS;
SELECT *
FROM information_schema.COLLATION_CHARACTER_SET_APPLICABILITY;

# 校对规则命名规定
# 语言名_?_(ci(大小写不敏感)|cs(大小写敏感)|bin(二元，比较是基于字符编码的值))
SHOW COLLATION LIKE 'utf8%';
SELECT 'A' COLLATE utf8_general_ci;
SELECT 'a' COLLATE utf8_general_ci;

# 比较A与a
SELECT CASE WHEN 'A' COLLATE utf8_general_ci = 'a' COLLATE utf8_general_ci
  THEN 1
       ELSE 0 END;

# MySql 字符集设置
# 级别：服务器级别、数据库、表级、字段级、客户端连接级别

# 服务器级别设置:默认是 latin1字符集
SHOW VARIABLES LIKE 'CHARACTER_SET_SERVER';
SHOW VARIABLES LIKE 'COLLATION_SERVER';

# 可以在my.cnf中配置，不指定校验规则，使用默认校验规则
# character-set-server=utf8
# 启动的时候指定
# mysql --character-set-server=utf8


# 数据库级别，create 和 alter指定
# 查看数据库字符集
SHOW VARIABLES LIKE 'CHARACTER_SET_DATABASE';
SHOW VARIABLES LIKE 'COLLATION_DATABASE';

# 例子：创建数据库，指定字符集，
DROP DATABASE IF EXISTS test_character;
CREATE DATABASE test_character
  CHARACTER SET gbk;
USE test_character;

# 查看某个数据库的字符集
SELECT
  *,
  default_character_set_name,
  DEFAULT_COLLATION_NAME

FROM information_schema.SCHEMATA
WHERE schema_name = 'cmspdat';

# 也可以使用alter 修改数据库字符集
# alter 数据库。已经存在数据，因为修改字符集并不能将已有的数据按照新的字符集进行存放，所以不能通过修改字符集直接修改数据的内容


# 表级别字符集。alter修改不会影响原有数据
CREATE TABLE learning.table_character (
  col INT
)
  ENGINE InnoDB
  CHARACTER SET gbk;

# 查看某表的字符集
SHOW CREATE TABLE learning.table_character;
SELECT *
FROM information_schema.TABLES
WHERE TABLE_NAME = 'emp';

# 列级别 。不常用。


# 客户端连接级别
# 客户端
SHOW VARIABLES LIKE 'character_set_client';
# 连接
SHOW VARIABLES LIKE 'character_set_connection';
# 返回结果
SHOW VARIABLES LIKE 'character_set_results';

# 设置方法：
# my.cnf配置：default-character-set=gbk
# 或者SET NAMES


# 字符集的修改
# alter 命令只会修改新的数据，旧数据不会改变
# 已有数据需要先导表，修改，导入

# 模拟将latin1 字符集的数据库修改成GBK字符集数据库
# 1、导出表结构：
# mysqldump -uroot -p --default-character-set=gbk -d learning > createtab.sql
# --default-character-set=gbk 表示设置连接字符集
# -d 表示只导出表结构

# 2、手工修改 createtab.sql 中表结构定义的字符集
# 3、确保记录不在更新，导出数据
# mysqldump -uroot -p --quick --no-create-info --extended-insert --default-character-set=latin1  learning > data.sql
# --quick:转存大的表
# --extended-insert 使用包括几个values列表的多行insert语法。使得文件更小，提高效率
# --no-create-info 不导出create 建表语句
# --default-character-set=latin1 按照原有字符集导出数据

# 4、打开data.sql,将set names latin1 修改成set names gbk
# 5、使用新的字符集新建数据库
CREATE DATABASE learning
  DEFAULT CHARSET gbk;

# 6、创建表，执行creattab.sql
# mysql -uroot -p learning < createtab.sql;

# 7、导入数据，执行data.sql
# mysql -uroot -p learning < data.sql;
