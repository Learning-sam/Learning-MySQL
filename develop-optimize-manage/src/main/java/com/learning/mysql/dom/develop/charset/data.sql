-- MySQL dump 10.13  Distrib 5.6.12, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: learning
-- ------------------------------------------------------
-- Server version	5.6.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES latin1 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `dept`
--

LOCK TABLES `dept` WRITE;
/*!40000 ALTER TABLE `dept` DISABLE KEYS */;
INSERT INTO `dept` VALUES (1,'sam'),(3,'fin'),(8,'tech');
/*!40000 ALTER TABLE `dept` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `emp`
--

LOCK TABLES `emp` WRITE;
/*!40000 ALTER TABLE `emp` DISABLE KEYS */;
INSERT INTO `emp` VALUES ('tom','2013-09-01',5000.00,1,NULL),('jack','2016-03-01',10000.00,3,NULL),('marry','2017-12-03',12000.00,3,NULL),('sam','2016-07-01',10000.00,NULL,30);
/*!40000 ALTER TABLE `emp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `new_table`
--

LOCK TABLES `new_table` WRITE;
/*!40000 ALTER TABLE `new_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `new_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t1`
--

LOCK TABLES `t1` WRITE;
/*!40000 ALTER TABLE `t1` DISABLE KEYS */;
INSERT INTO `t1` VALUES (1.23,1.23,1.23),(1.234,1.234,1.23),(1.234,1.234,1.23);
/*!40000 ALTER TABLE `t1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t2`
--

LOCK TABLES `t2` WRITE;
/*!40000 ALTER TABLE `t2` DISABLE KEYS */;
/*!40000 ALTER TABLE `t2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t3`
--

LOCK TABLES `t3` WRITE;
/*!40000 ALTER TABLE `t3` DISABLE KEYS */;
/*!40000 ALTER TABLE `t3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t4`
--

LOCK TABLES `t4` WRITE;
/*!40000 ALTER TABLE `t4` DISABLE KEYS */;
INSERT INTO `t4` VALUES ('2017-07-20','23:43:00','2017-07-20 23:43:00','2017-07-20 15:43:00','2017-07-20 15:43:00'),('2017-07-20','23:49:38','2017-07-20 23:49:38','2017-07-20 15:49:38','2017-07-20 15:49:38');
/*!40000 ALTER TABLE `t4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t5`
--

LOCK TABLES `t5` WRITE;
/*!40000 ALTER TABLE `t5` DISABLE KEYS */;
INSERT INTO `t5` VALUES ('M'),('F'),('M'),(NULL),('M'),('F'),('M'),(NULL);
/*!40000 ALTER TABLE `t5` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t7`
--

LOCK TABLES `t7` WRITE;
/*!40000 ALTER TABLE `t7` DISABLE KEYS */;
/*!40000 ALTER TABLE `t7` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `table_character`
--

LOCK TABLES `table_character` WRITE;
/*!40000 ALTER TABLE `table_character` DISABLE KEYS */;
/*!40000 ALTER TABLE `table_character` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `test_float`
--

LOCK TABLES `test_float` WRITE;
/*!40000 ALTER TABLE `test_float` DISABLE KEYS */;
INSERT INTO `test_float` VALUES (1.12,NULL),(1.12,NULL),(1.12,NULL),(1.16,NULL),(131072.31,131072.32);
/*!40000 ALTER TABLE `test_float` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `test_text`
--

LOCK TABLES `test_text` WRITE;
/*!40000 ALTER TABLE `test_text` DISABLE KEYS */;
INSERT INTO `test_text` VALUES (1,'suzhousuzhou','ba2dd8f1c05c52a8348f95ca459b92ca'),(2,'suzhousuzhou','ba2dd8f1c05c52a8348f95ca459b92ca'),(3,'suzhou 2017suzhou 2017','897bfe0e37900eb5582af954bd594f66');
/*!40000 ALTER TABLE `test_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `test_varchar`
--

LOCK TABLES `test_varchar` WRITE;
/*!40000 ALTER TABLE `test_varchar` DISABLE KEYS */;
INSERT INTO `test_varchar` VALUES ('',''),('ab','ab'),('abcd','abcd'),('ab','ab  ');
/*!40000 ALTER TABLE `test_varchar` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-24 18:59:07
