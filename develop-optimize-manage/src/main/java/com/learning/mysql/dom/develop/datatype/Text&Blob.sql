# text(字符数据) blob(二进制)
# 大数据

# text与blob 在大量删除操作的性能问题，数据表空洞，定期使用OPTIMIZE TABLE进行碎片整理
DROP TABLE IF EXISTS learning.test_text;
CREATE TABLE learning.test_text (
  id      INT,
  content TEXT
);

# repeat函数：插入大量字符串
INSERT INTO learning.test_text VALUES (1, repeat('haha', 100));
# 蠕虫
INSERT INTO learning.test_text SELECT *
                               FROM learning.test_text;
SELECT *
FROM learning.test_text;

# 查看文件大小
# cd /data/soft/mysql-5.6.12/data/learning
# du -sh test_text.*

# 删完数据，然后在查看大小，没有变小
DELETE FROM learning.test_text;

# 优化表操作，再次查看大小已经变化
OPTIMIZE TABLE learning.test_text;

# 使用合成索引(对大文本做散列值，然后精确查询即可)提高大文本的查询性能
DROP TABLE IF EXISTS learning.test_text;
CREATE TABLE learning.test_text (
  id         INT,
  content    TEXT,
  hash_value VARCHAR(40)
);


INSERT INTO learning.test_text VALUES (1, repeat('suzhou', 2), md5(content));
INSERT INTO learning.test_text VALUES (2, repeat('suzhou', 2), md5(content));
INSERT INTO learning.test_text VALUES (3, repeat('suzhou 2017', 2), md5(content));

# 利用hash值搜索，只能精确匹配
SELECT *
FROM learning.test_text
WHERE test_text.hash_value = md5(repeat('suzhou', 2));

# 模糊查询，可以利用前缀索引,对context的前100个字符进行模糊查询
CREATE INDEX idx_text
  ON learning.test_text (content(100));

# % 只能放在最后
DESC SELECT *
     FROM learning.test_text
     WHERE test_text.content LIKE 'suzhou%';

# 注意
# 避免检索大数据。例如 select *
# 把大数据放入第二张表，原表数据列转为固定长度