# char(定长)与varchar(非定长)
# 小数据

DROP TABLE IF EXISTS learning.test_varchar;
CREATE TABLE learning.test_varchar (
  name  CHAR(4),
  name2 VARCHAR(4)
);

# char 固定是4个字符，不够会凑够
INSERT INTO learning.test_varchar VALUES ('', ''), ('ab', 'ab'), ('abcd', 'abcd');
SELECT *
FROM learning.test_varchar;

# 严格模式:超出大小无法插入
INSERT INTO learning.test_varchar VALUES ('abcddfg', 'abcdefg');

# 检索的差别:char 会把尾部的空格删除
INSERT INTO learning.test_varchar VALUES ('ab  ', 'ab  ');

SELECT
  concat(name, '+'),
  concat(name2, '+')
FROM learning.test_varchar;

# 使用，不同的引擎不一样
# InnoDB:VARCHAR
