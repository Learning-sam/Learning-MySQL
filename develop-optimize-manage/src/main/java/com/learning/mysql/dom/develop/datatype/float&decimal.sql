# 浮点数与定点数(字符串保存，更加精确)

DROP TABLE IF EXISTS learning.test_float;
CREATE TABLE learning.test_float (
  sal  FLOAT(8, 2),
  sal2 DECIMAL(8, 2)
);

DESC learning.test_float;

# 可以插入，但是会四舍五入，1.12,1.16
INSERT INTO learning.test_float VALUES (1.123, NULL), (1.155, NULL);

# 单精度浮点数，产生了误差 131072.31, 131072.32
# 浮点数的运算也会产生误差
INSERT INTO learning.test_float VALUES (131072.32, 131072.32);

SELECT *
FROM learning.test_float;