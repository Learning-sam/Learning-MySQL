# 查看表引擎
# TokuDB/Infobright 第三方插件索引


# 查看当前数据库支持引擎,以及默认引擎
SHOW ENGINES;
SELECT *
FROM information_schema.ENGINES;

# 查看某张表的引擎
# 查看建表语句
SHOW CREATE TABLE learning_mysql.emp;

# 从 information_schema.TABLES 表获取
SELECT *
FROM information_schema.TABLES
WHERE TABLE_SCHEMA = 'learning_mysql'
      AND TABLE_NAME = 'emp';

# 建表指定引擎
DROP TABLE IF EXISTS learning.new_table;
CREATE TABLE learning.t6 (
  id INT NOT NULL PRIMARY KEY  AUTO_INCREMENT
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


CREATE TABLE learning.t7 (
  id INT NOT NULL PRIMARY KEY  AUTO_INCREMENT
)
  ENGINE = MyISAM
  DEFAULT CHARSET = utf8;

SHOW CREATE TABLE learning.t7;

# 修改表引擎
ALTER TABLE learning.t7
  ENGINE = InnoDB;

# InnoDB


# 在数据量极大的时候(大于1亿条的级别),InnoDB的B+树性能的缺陷会暴露,
# 这时MySQL的DBA可能会转向TokuDB这个第三方开源的MySQL引擎来处理这些大数据



