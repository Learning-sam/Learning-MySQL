USE learning;
DROP TABLE IF EXISTS learning.test_index;
CREATE TABLE learning.test_index (
  id   INT NOT NULL,
  name VARCHAR(10)
);
# 建索引
CREATE INDEX idx_test_index_id
  ON learning.test_index (id);

SHOW CREATE TABLE learning.test_index;

# 查看是否使用了索引
DESC SELECT *
     FROM learning.test_index
     WHERE id = 1;
EXPLAIN SELECT *
        FROM learning.test_index
        WHERE id = 1;

# 删除索引
DROP INDEX idx_test_index_id
ON learning.test_index;

# 索引的原则
# 1、搜索索引列：where 语句后面的列
# 2、使用唯一索引。索引的列基数越大，索引的效果越好。例如存放生日的列具有不同的值，很容易区分各行。而记录性别的列，只含有M和F，使用索引就没有多大用处。
# 3、使用端索引。如果对字符串进行索引，应该制定一个前缀。如一个char(200)列，如果前20或者10个字符内，多数值是唯一的就可以这么用
# 4、利用最左前缀。在创建一个n列的索引时，实际是创建了mysql可利用的n个索引。可以利用索引中最左边的列集来匹配
# 5、不要过度索引。占用磁盘空间、影响写入速度
# 6、InnoDB引擎的表，记录默认会按照一定的顺序保存，优先级：主键>唯一索引>内部列。尽量自己制定主键


# BTREE索引与HASH索引
# 1、BTREE索引 ，可以使用>、<、>=、<=、between、!=、<>、like 'pattern'(pattern 不以通配符开始)
# 2、HASH索引，只能使用=或者<=>、IN操作符的等式比较；Hash 索引无法被用来避免数据的排序操作。

CREATE INDEX idx_key
  ON learning.test_index (id) USING BTREE;
CREATE INDEX idx_key
  ON learning.test_index (id) USING HASH;

# 创建索引的方式
DROP TABLE IF EXISTS learning.test_index;
CREATE TABLE learning.test_index (
  id   INT PRIMARY KEY NOT NULL,
  name VARCHAR(20) UNIQUE,
  code VARCHAR(2),
  desc TEXT,
  KEY `idx_test_index_id` (`id`) USING BTREE
);
CREATE FULLTEXT INDEX idx_test_index_desc
  ON learning.test_index (desc);














