# 分区
SELECT schema();
# 是否支持分片
SHOW VARIABLES LIKE '%partition%';
SHOW PLUGINS;

# 新建6个hash分区的表
USE learning;
DROP TABLE IF EXISTS learning.partition_emp;
CREATE TABLE learning.partition_emp (
  id   INT PRIMARY KEY,
  name VARCHAR(10),
  sal  INT
)
  ENGINE InnoDB PARTITION BY HASH (id) PARTITIONS 6;

SHOW CREATE TABLE learning.partition_emp;

# 分区适用于一个表的所有数据与索引
# 分区表上创建的索引一定是本地 LOCAL 索引


# 分区类型
# RANGE: 基于一个给定连续区间范围，把数据分配到不同的分区
# LIST: 类似RANGE分区，区别在于list分区是基于枚举出的值列表分区 RANGE 是基于给定的连续区间范围
# HASH: 给予给定的分区个数，把数据分配到不同的分区
# KEY: 类似与hash 分区

# RANGE 分区
# 可以使用 PARTITION BY RANGE (sal)  ,如YEAR()等
# PARTITION BY RANGE COLUMNS (sal),不需要函数，直接使用
# 分区键如果是 null 值，会被当成一个最小的值来处理
#
DROP TABLE IF EXISTS learning.partition_emp;
CREATE TABLE learning.partition_emp (
  id   INT,
  name VARCHAR(10),
  sal  INT
)
  ENGINE = myisam
  DEFAULT CHARSET = utf8
  PARTITION BY RANGE (sal) (        -- 指定分区方式,有主键，必须主键
  PARTITION p0 VALUES LESS THAN (10), -- 分了两个区，如果插入大于20，会报错
  PARTITION p1 VALUES LESS THAN (20),
  PARTITION p3 VALUES LESS THAN MAXVALUE -- 加了这句，大于的都可以插入这个分区
  );

INSERT INTO learning.partition_emp (id, name, sal) VALUES (1, 'sam', 8);
# -- [HY000][1526] Table has no partition for value 28
INSERT INTO learning.partition_emp (id, name, sal) VALUES (1, 'sam', 28);

SELECT *
FROM learning.partition_emp;

# 删除 RANGE 分区
ALTER TABLE learning.partition_emp
  DROP PARTITION p1;

#

# LIST 分区 ，分区列为INT
DROP TABLE IF EXISTS learning.partition_emp;
CREATE TABLE IF NOT EXISTS learning.partition_emp (
  id   INT NOT NULL PRIMARY KEY,
  name VARCHAR(10),
  sal  INT
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  PARTITION BY LIST (id) (
  PARTITION p1 VALUES IN (1, 3, 5),
  PARTITION p2 VALUES IN (2, 4, 6)
  );

# 有主键必须是分区键，分区键为飞INT，需要加 COLUMNS
CREATE TABLE IF NOT EXISTS learning.partition_emp (
  id   INT NOT NULL,
  name VARCHAR(10),
  sal  INT
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  PARTITION BY LIST COLUMNS (name) (
  PARTITION list_p1 VALUES IN ('sam', 'tom', 'dan'),
  PARTITION list_p2 VALUES IN ('eason', 'hebe')
  );

# COLUMNS 分区
# 支持多数据数据类型
# 多列分区
DROP TABLE IF EXISTS learning.partition_emp;
CREATE TABLE IF NOT EXISTS learning.partition_emp (
  id  INT NOT NULL,
  sal INT
)PARTITION BY RANGE COLUMNS (id, sal) (
PARTITION p01 VALUES LESS THAN (0, 10),
PARTITION p02 VALUES LESS THAN (10, 10),
PARTITION p03 VALUES LESS THAN (20, 20),
PARTITION p04 VALUES LESS THAN (30, 30),
PARTITION p05 VALUES LESS THAN ( MAXVALUE, MAXVALUE )
);

# 查看表状态
SHOW CREATE TABLE learning.partition_emp;
SHOW TABLE STATUS;

# 插入数据,会存入这边 (10,10)
INSERT INTO learning.partition_emp (id, sal) VALUES (1, 10);
INSERT INTO learning.partition_emp (id, sal) VALUES (10, 12);

# 查看数据在哪个分区
SELECT *
FROM information_schema.PARTITIONS
WHERE TABLE_NAME = 'partition_emp';


SELECT
  partition_name,
  partition_expression,
  partition_description,
  table_rows
FROM information_schema.partitions
WHERE
  table_schema = schema()
  AND table_name = 'partition_emp';

# HASH分区
# 分散热点读，对分区键进行散列函数
# 两种hash分区：常规(取模)与线性(线性的2的幂的运算法则)
# 分区键只能是整数
DROP TABLE IF EXISTS learning.partition_emp;
CREATE TABLE IF NOT EXISTS learning.partition_emp (
  id  INT NOT NULL,
  sal INT
) PARTITION BY HASH (id) PARTITIONS 4;

# 插入数据234 ，MOD(234,4)=2，保存在第二个分区
INSERT INTO learning.partition_emp (id, sal) VALUES (234, 12);
# 取模
SELECT MOD(234, 4);

# 查看
EXPLAIN SELECT *
        FROM learning.partition_emp
        WHERE partition_emp.id = 234;

# PARTITION BY HASH (expr) PARTITIONS 4 , expr可以是非常用和非随机数。但是不建议使用复杂表达式，因为每次的操作都需要重新计算，会小号性能。
# 常规hash问题：增加分区和合并分区：
# 1、如原来有5个分区，取模为MOD(expr,5),根据语数0~4,分布在5个分区之中。
# 2、现在增加一个分区，MOD(expr,6)，根据余数0~5分区在6个分区中，原来大部分数据都需要重新计算重新分区。代价太大。
# 线性分区解决了这个问题

DROP TABLE IF EXISTS learning.partition_emp;
CREATE TABLE IF NOT EXISTS learning.partition_emp (
  id  INT NOT NULL,
  sal INT
) PARTITION BY LINEAR HASH (id) PARTITIONS 4;

# 插入数据234 ，MOD(234,4)=2，保存在第二个分区
INSERT INTO learning.partition_emp (id, sal) VALUES (234, 12);

EXPLAIN SELECT *
        FROM learning.partition_emp
        WHERE partition_emp.id = 234;

# 计算分区:2的线性次幂
# V = Power(2, Ceiling(log(2,num))):大于等于2的整数的幂，即2的多少次方
# F(column_list)&(V-1)
SELECT log(2, 4); -- 2
SELECT ceiling(log(2, 4)); -- 2
# POWER（number,power）返回给定数字的乘幂。
SELECT power(2, ceiling(log(2, 4)));

# 线性HASH 分区的优点，分区维护效率更高，缺点，数据分布不太均衡

# Key 分区
# 1、HASH 支持函数，只能是整数键
# 2、key 分区，只能使用hash函数，支持除了blob、text类型外的其他类型
# 3、不指定分区键，默认为主键，如果没有会选择非空唯一键作为分区键（非空）;有主键，则必须是主键为分区键
DROP TABLE IF EXISTS learning.partition_emp;
CREATE TABLE IF NOT EXISTS learning.partition_emp (
  id   INT NOT NULL,
  name VARCHAR(10),
  sal  INT
) PARTITION BY KEY (name) PARTITIONS 4;

# 默认主键为分区键，
DROP TABLE IF EXISTS learning.partition_emp;
CREATE TABLE IF NOT EXISTS learning.partition_emp (
  id   INT NOT NULL PRIMARY KEY,
  name VARCHAR(10),
  sal  INT
) PARTITION BY KEY () PARTITIONS 4;

# 子分区（复合分区）:分区表中对每个分区的再次分区
# 对已经通过range、list分区的表再进行子分区（hash、key）
DROP TABLE IF EXISTS learning.partition_emp;
CREATE TABLE IF NOT EXISTS learning.partition_emp (
  id   INT,
  name VARCHAR(10),
  sal  INT
) PARTITION BY RANGE (id)
SUBPARTITION BY HASH (id) SUBPARTITIONS 2 (
PARTITION p0 VALUES LESS THAN (10),
PARTITION p1 VALUES LESS THAN (100),
PARTITION p2 VALUES LESS THAN MAXVALUE
);

# 分区对于分区键为 NULL 值的处理
# 分区键可以为 NULL：但是会被当做最小值或者0
# range中为0，hash中为0，list中必须出现在枚举中不然不被接受
# 建议设置为非空或者默认值来绕开NULL的处理


# 分区管理: 添加、删除、重定义、合并、拆分 ALTER TABLE
# range\list分区管理
# 删除分区，会删除分区的数据
ALTER TABLE learning.partition_emp
  DROP PARTITION p0;

# 新增一个分区
# range:只能新增分区到分区列表的最大一端
ALTER TABLE learning.partition_emp
  ADD PARTITION (PARTITION p4 VALUES LESS THAN (200));

# 新增list的分区，不能添加一个包含现有分区值列表中的任意值的分区。
# 一个固定的分区键值，必须指定并且只能制定一个唯一的分区
ALTER TABLE learning.partition_emp
  ADD PARTITION (PARTITION p4 VALUES IN (12));

# 重新定义分区1:拆分，重新划分p3分区为两个分区p2和p3
ALTER TABLE learning.partition_emp
  REORGANIZE PARTITION p3
  INTO (PARTITION p2 VALUES LESS THAN (200),
  PARTITION p3 VALUES LESS THAN (400));

# 重新定义分区2:合并，两个分区p1、p2、p3为p3分区
ALTER TABLE learning.partition_emp
  REORGANIZE PARTITION p1, p2, p3
  INTO (PARTITION p3 VALUES LESS THAN (200));

# hash\key分区管理
# 减少hash 分区的数量，从4个分区变为2个分区
ALTER TABLE learning.partition_emp
  COALESCE PARTITION 2;

# 增加区分(暂时无效)
# ALTER TABLE learning.partition_emp ADD PARTITION ()  PARTITIONS;







