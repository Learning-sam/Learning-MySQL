USE learning;
-- 事件调度器
# 时间调度器
# ON SCHEDULE 指定事件在何时执行及执行频次
# DO 执行的具体操作
# 创建的时候执行一次，然后每隔1小时出发一次
CREATE EVENT myEvent
  ON SCHEDULE AT current_timestamp + INTERVAL 1 HOUR
DO
  UPDATE learning.emp
  SET age = age + 1;

# 每隔5秒插入一条数据的调度器
ALTER TABLE learning.dept
  DROP PRIMARY KEY;
DROP EVENT IF EXISTS v_dept_insert;
CREATE EVENT IF NOT EXISTS v_dept_insert
  ON SCHEDULE EVERY 5 SECOND
DO
  INSERT INTO learning.dept VALUES (11, 'event_test');

# 查看
SHOW EVENTS;
SELECT *
FROM information_schema.EVENTS;

# 默认是关闭的
SHOW VARIABLES LIKE '%scheduler%';
# 设置开启
SET GLOBAL EVENT_SCHEDULER = 0;

# 进程列表，设置打开以后会有 event_scheduler 进程
SHOW PROCESSLIST;

# 防止数据过大，创建看另一个定时任务
DROP EVENT IF EXISTS v_dept_del;
CREATE EVENT IF NOT EXISTS v_dept_del
  ON SCHEDULE EVERY 1 MINUTE
  ON COMPLETION PRESERVE DISABLE
DO
  DELETE FROM learning.dept;

SELECT *
FROM learning.dept;

# 调度器的选项
# ON COMPLETION PRESERVE DISABLE：创建后并不生效

# 开启定时任务
ALTER EVENT v_dept_del
ON COMPLETION PRESERVE ENABLE;
# 关闭定时任务
ALTER EVENT v_dept_del
ON COMPLETION PRESERVE DISABLE;
