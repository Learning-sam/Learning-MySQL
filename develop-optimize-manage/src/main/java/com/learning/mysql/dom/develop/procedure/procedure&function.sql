# Mysql的存储过程和函数中允许包含DDL语句，允许commit、rollback
# 存储过程：没有返回值，参数可以是IN、OUT、INOUT
# 函数:必须有返回值，参数类型只能是IN


# 创建：没有 OR REPLACE ，只能alter修改
# DELIMITER $$;
# DELIMITER ;;
CREATE PROCEDURE learning.film_in_stock(IN p_ename VARCHAR(10), OUT p_sal INT)
READS SQL DATA
  BEGIN
    SELECT sal
    INTO p_sal
    FROM learning.emp
    WHERE ename = p_ename;
  END;

CALL learning.film_in_stock('sam', @a);
SELECT @a;

# 修改结束符，避免执行';'结束
# DELIMITER $$
# DELIMITER ;


# 权限问题：以创建者的权限执行
# 1、root创建存储过程p,查询t表数据
# 2、sam 有执行p权限，但是没有查询t的权限
# 3、select t 不行，但是可以执行p可以，因为执行p是以创建者的权限执行的。


# 删除
DROP PROCEDURE IF EXISTS learning.film_in_stock;

# 查看状态
SHOW PROCEDURE STATUS LIKE 'film_in_stock';
# 查看定义
SHOW CREATE PROCEDURE learning.film_in_stock;
# 查看information_schema.ROUTINES
SELECT *
FROM information_schema.ROUTINES;

# 变量
DROP FUNCTION IF EXISTS learning.p_declare;
CREATE FUNCTION learning.p_declare()
  RETURNS VARCHAR(10)
DETERMINISTIC
READS SQL DATA
  BEGIN
    DECLARE p_result VARCHAR(20);
    SELECT ename
    INTO p_result
    FROM learning.emp
    WHERE sal = 5000;
    RETURN p_result;
  END;
SELECT learning.p_declare();

# 条件的定义与使用
ALTER TABLE learning.dept
  ADD PRIMARY KEY (deptno);

# 这边执行到插入主键重复异常:[2017-07-25 16:09:20] [23000][1062] Duplicate entry '1' for key 'PRIMARY'
DROP PROCEDURE IF EXISTS learning.p_dept_insert;
CREATE PROCEDURE learning.p_dept_insert()
  BEGIN
    SET @x = 1;
    INSERT INTO learning.dept VALUES (2, 'p_dept_insert');
    SET @x = 2;
    INSERT INTO learning.dept VALUES (1, 'Test');
    SET @x = 3;
  END;

CALL learning.p_dept_insert();
SELECT @x;

# 对主键重复的异常进行处理:
# 23000是查询状态,这个condition_value，也可以设置成mysql-erroe-code(1062),或者SQLWARNING\NOT FOUND\SQLEXCEPTION
# CONTINUE，会继续执行下面的语句

DROP PROCEDURE IF EXISTS learning.p_dept_insert;
CREATE PROCEDURE learning.p_dept_insert()
  BEGIN

    DECLARE CONTINUE HANDLER FOR SQLSTATE '23000' SET @x2 = 1;
    #     DECLARE CONTINUE HANDLER FOR 1062 SET @x2 = 1;
    SET @x = 1;
    INSERT INTO learning.dept VALUES (2, 'p_dept_insert');
    SET @x = 2;
    INSERT INTO learning.dept VALUES (1, 'Test');
    SET @x = 3;
  END;

CALL learning.p_dept_insert();

# 多种设置
# 查询状态
# DECLARE CONTINUE HANDLER FOR SQLSTATE '23000' SET @x2 = 1;

# 捕获mysql-erroe-code
# DECLARE CONTINUE HANDLER FOR 1062 SET @x2 = 1;

# 事先定义
# DECLARE aa CONDITION FOR SQLSTATE '23000'
# DECLARE CONTINUE HANDLER FOR aa SET @x2 = 1;

# 捕获SQLEXCEPTION
# DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET @x2 = 1;


# 光标使用：对结果集进行循环的处理

# 申明\OPEN\FETCH\CLOSE
# DECLARE c_name CURSOR FOR SELECT_statement;
DROP PROCEDURE IF EXISTS learning.p_emp;
CREATE PROCEDURE learning.p_emp()
  BEGIN
    DECLARE p_ename VARCHAR(10);
    DECLARE p_age INT;
    DECLARE c_user CURSOR FOR SELECT
                                ename,
                                age
                              FROM learning.emp;

    DECLARE EXIT HANDLER FOR NOT FOUND CLOSE c_user;

    SET @x1 = 0;
    SET @x2 = 0;

    OPEN c_user;

    REPEAT
      FETCH c_user
      INTO p_ename, p_age;
      IF p_ename = 'sam'
      THEN
        SET @x1 = p_ename + '-' + p_age;
      END IF;
    UNTIL 0 END REPEAT;

    CLOSE c_user;

  END;

CALL learning.p_emp();

SELECT
  @x1,
  @x2;


# 流程控制：if\loop\leave\repeat\while\iterate






