# 安全：SQL注入与其他防范措施
USE learning;
DROP TABLE IF EXISTS learning.security_user;
CREATE TABLE IF NOT EXISTS learning.security_user (
  userId   INT(11)     NOT NULL AUTO_INCREMENT,
  userName VARCHAR(20) NOT NULL DEFAULT '',
  password VARCHAR(20) NOT NULL DEFAULT '',
  PRIMARY KEY (userId)
)
  ENGINE = InnoDB
  CHARACTER SET utf8;

INSERT INTO security_user (userName, password) VALUES ('sam', '123456');




