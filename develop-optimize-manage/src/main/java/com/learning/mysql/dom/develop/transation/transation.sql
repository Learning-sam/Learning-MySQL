# 事务
# MyISAM、MEMORY：表级锁定
# BMB：页级锁定
# InnoDB：行及锁定

USE learning;
# LOCK TABLES :锁定当前线程的表
# UNLOCK TABLES：释放当前线程获取的任何锁

# 表锁与释放锁的简单例子
# 1、seeesion_1 获取表emp 的read 锁定
LOCK TABLES learning.emp READ;

# 2、当前session_1 可以查询表记录，其他session_2也可以查询表记录
SELECT *
FROM emp;

# 3、session_2 更新锁定表会等待获取获得锁，等待中
UPDATE emp
SET ename = 'sam';

# 4、session_1释放所，这个时候，session_2继续等待
UNLOCK TABLES;

# 5、session_2获得锁，更新操作完成。
UPDATE emp
SET ename = 'sam';

# 事务控制
# 1、START TRANSACTION \ BEGIN 都可以手动开始一个事务
# 2、永久开启手动事务： SET AUTOCOMMIT = 0;
# 3、COMMIT \ ROLLBACK 提交事务与回滚事务
# 4、CHAIN 事务提交或者回滚以后，CHAIN 是立即启动一个新事物，与刚才的事务有相同的事务
# 5、RELEASE 事务提交或者回滚以后，断开与客户端的连接
# 6、在所标期间，用 START TRANSACTION 命令开始一个事务，会造成隐含的 UNLOCK TABLES;
# 7、在同一个事务中。最好不要使用不同存储引擎的表。否则 ROLLBACK 时需要对非事务类型的表进行特别的处理。因为 COMMIT \ ROLLBACK 只能对事务类型的表进行提交和回滚
# 8、DDL语句是不能回滚的，会有隐式的提交
# 9、SAVEPOINT 指定回滚事务的一部分ROLLBACK TO SAVEPOINT，RELEASE SAVEPOINT删除SAVEPOINT

# 查看事务属性
SHOW VARIABLES LIKE 'AUTOCOMMIT';

# 分布式事务：只支持InnoDB
# 涉及多个资源管理器（RM）和一个事务管理器（TM）
# XA {START | BEGIN } xid [join | resume]
