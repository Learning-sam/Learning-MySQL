USE learning;
# 触发器
# CREATE TRIGGER trigger_name trigger_time trigger_event ON tbl_name FOR EACH ROW trigger_stmt
# 新建一个触发器
DROP TRIGGER IF EXISTS t_dept_ins;
CREATE TRIGGER t_dept_ins
AFTER INSERT ON learning.dept FOR EACH ROW
  BEGIN
    INSERT INTO learning.emp VALUES ('trigger', '2012-12-1', 20000, 1, 32);
  END;

# 测试
INSERT INTO learning.dept VALUES (9, 'snow');
SELECT *
FROM learning.dept;
SELECT *
FROM learning.emp;

# trigger_name:时机
# trigger_event:事件
# AFTER INSERT \ BEFORE INSERT \ AFTER UPDATE \ BEFORE UPDATE

# 查看
SHOW TRIGGERS;
SELECT *
FROM information_schema.TRIGGERS;

# 触发器的使用
# 1、不能将数据返回客户端，但是可以和存储过程配合使用
# 2、在触发器中使用事务语法



