# 创建视图
CREATE OR REPLACE VIEW emp_dept_list_view AS
  SELECT
    b.ename,
    b.age,
    b.hiredate,
    b.sal,
    b.deptno,
    a.deptname
  FROM learning.dept AS a, learning.emp AS b
  WHERE a.deptno = b.deptno
WITH CASCADED CHECK OPTION;

SELECT *
FROM learning.emp_dept_list_view;

# 插入数据，会导致视图更新
INSERT INTO learning.emp VALUES ('new_user', '2014-09-23', 2333, 3, 30);

# 视图的可更新性和视图中查询的定义有关系：以下为不可更新
# 1、包含聚合函数
# 2、常量视图
# 3、select 中包含子查询

# 决定是否允许更新数据的条件
# WITH CASCADED CHECK OPTION：满足所有针对该视图的所有视图的条件才可以更新
# WITH LOCAL CHECK OPTION:只要满足本视图天剑就可以更新


# 修改视图
ALTER VIEW learning.emp_dept_list_view AS
SELECT
  b.ename,
  b.age,
  b.hiredate,
  b.sal,
  b.deptno,
  a.deptname
FROM learning.dept AS a, learning.emp AS b
WHERE a.deptno = b.deptno
WITH CASCADED CHECK OPTION;

# 删除索引
DROP VIEW IF EXISTS learning.emp_dept_list_view;

# 查看视图
SHOW TABLES FROM learning;
SHOW TABLE STATUS FROM learning WHERE Comment = 'view';
SHOW CREATE VIEW learning.emp_dept_list_view;
SELECT *
FROM information_schema.VIEWS WHERE TABLE_SCHEMA='learning' AND TABLE_NAME='emp_dept_list_view';

