# 日志

# 错误日志
# mysqld --log-error[=file_name] 指定保存错误日志文件的位置
# 默认：$DATADIR/host_name.err (host_name 为主机名)


# BINLOG 二进制日志
# https://dev.mysql.com/doc/refman/5.7/en/mysqlbinlog-row-events.html

# --log-bin[=file_name]
# 默认名为主机名后面跟 host_name-bin
# my.cof中配置地址 bin-log = ....


# 日志的读取:默认是ROW格式的
# mysqlbinlog bin-log.000008

USE learning;
CREATE TABLE bin_log
(
  id   INT         NOT NULL,
  name VARCHAR(20) NOT NULL,
  date DATE        NULL
)
  ENGINE = InnoDB;

START TRANSACTION;
INSERT INTO bin_log VALUES (1, 'apple', NULL);
UPDATE bin_log
SET name = 'pear', date = '2009-01-01'
WHERE id = 1;
DELETE FROM bin_log
WHERE id = 1;
COMMIT;

# FLUSH LOGS;

mysqlbinlog bin-log.000008
# mysqlbinlog -vv bin-log.000008
# mysqlbinlog -v bin-log.000008
# mysqlbinlog -v --base64-output=DECODE-ROWS bin-log.000008


# 日志的删除
# 方式一：reset master;
ls -ltr bin-log*
RESET MASTER;
ls -ltr bin-log*

# 方式二：PURGE MASTER LOGS TO XXX 删除 XXX 之前的日志
PURGE MASTER LOGS TO 'bin-log.000008';

# 方式三：PURGE MASTER LOGS BEFORE '日期' 删除该日期之前的所有日志
PURGE MASTER LOGS BEFORE 'yyyy-mm-dd hh24:mi:ss';

# 方式四：设置过期天数。 --expire_logs_days=3 需要重启mysql服务


# 查询日志:记录了客户端的所有语句(建议关闭)
# 启用查询日志保存:默认输出到文件file
# --log-output[=value] ,value 的值可以是 table、file、none中的一个或者多个，用逗号进行分割。表值得是general_log(慢查询日志为slow_log)

# 启用查询日志查询:
# --general_log[=0|1] 和 --general_log=file_name 进行控制

# SET GLOBAL SQL_LOG_OFF = ON;
# SET SQL_LOG_BIN ON;


# 慢查询日志：执行时间超过参数long_query_time(秒)并且扫描记录数不小于 min_examined_row_limit 的所有sql语句的日志
# 默认情况下,两类语句不会记录,管理语句和不使用索引进行查询的语句


