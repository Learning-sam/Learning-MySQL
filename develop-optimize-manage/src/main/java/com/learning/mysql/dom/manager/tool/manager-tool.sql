# mysql中的常用工具


# 连接服务器:服务器\端口\用户名\密码
# mysql 客户端连接工具
# mysql -h 192.168.1.1 -p 3306 -uroot -p

# 登录的用户
SELECT current_user();

# 设置连接字符集
# mysql -h 192.168.1.1 -p 3306 -uroot -p --default-character-set=utf8
# SET NAMES CHARSET;

SHOW VARIABLES LIKE 'character%';

# 执行选项：-e --excute = name
# mysql -u root -p root -e "select user,host from user"

# 格式化选项
# -E,--vertical: 将输出方式按照字段顺序竖着显示
# -s,--silent: 去掉mysql 中的线条框显示

# 错误处理选项
# -f,--force :强制执行SQL
# -v,--verbose :显示更多信息
# --show-warnings: 显示警告信息



# myisampack :MyISAM 表压缩工具

# mysqladmin :MySQL 管理工具
# mysqladmin -uroot -p shutdown;-- 关闭数据库


# mysqlbinlog:日志管理工具
# 具体用法：mysqlbinlog 【options】【log-files1】【log-files2】

# 【options】选项
# -d,--database=name:指定数据库
# -o,--offset=#:忽略掉日志中的前n行命令
# -r,--result-file=name:将输出的文本格式日志输出到指定文件
# -s,--short-form:显示简单格式
# --set-charset=char-name:在输出为文本格式时，在文件第一行加上set names char-name,这个选项在某些情况下装载市局时非常有用
# --start-datetime=name -stop-datetime=name:指定日期间隔内的所有日志
# --start-position=# --stop-position=#:指定位置间隔内的所有日志


# mysqlcheck: MyISAM 表维护
# mysqlcheck -uroot -p -c learning_mysql emp
# mysqlcheck -uroot -p -r learning_mysql emp
# mysqlcheck -uroot -p learning_mysql emp --auto-repair


# mysqldump 数据导出工具

# 字符集选项:在导出数据库的时候非常重要
# --default-character-set=name
# mysqld --verbose --help | grep 'default-character-set' | grep -v name

# 其他选项：
# -F, --flush-logs 备份前刷新
# -l, --lock-tables: 给所有表加读锁


# mysqlhotcopy MyISAM 表热备份工具


# mysqlimport 数据导入工具


# mysqlshow 数据库对象查看工具

# perror 错误代码查看工具

# replace 文本替换工具

use learning_mysql;
show tables ;



check table emp;
repair table emp;
analyze table emp;
optimize table emp;


























