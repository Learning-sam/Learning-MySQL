# 优化表的数据类型
USE sakila;

SELECT *
FROM customer
PROCEDURE analyse();


# 通过拆分提高表的访问效率
# 1-垂直拆分
# 2-水平拆分

# 使用中间表提高统计查询速度
use learning;

# 先复制数据到中间表，再在中间表统计数据
