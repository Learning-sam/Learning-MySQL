# MySQL 体系结构
# MySQL 有7组后台线程

# 查看后台线程的状态
SHOW ENGINE innodb STATUS;

# MySQL 内存管理及优化

# MyISAM 内存优化
# MyISAM 存储引擎使用 key buffer 缓存索引块，以加速 MyISAM 索引的读写速度。对于MyISAM 表的数据块，MyISAM没有特别的缓存机制，完全依赖于操作系统的IO缓存。

# 1- key_buffer_size
# key_buffer_size 决定 MyISAM 索引块缓存区的大小，直接影响 MyISAM 表的存取效率。建议1/4 可用内存分配
# key_buffer_size = 4G

# 评估索引缓存的效率
# key_reads/key_read_requests 应该小于0.01
SHOW STATUS LIKE 'key_read_requests';
SHOW STATUS LIKE 'key_reads';

# key_writes/key_write_requests 也应尽可能小
SHOW STATUS LIKE 'key_write_requests';
SHOW STATUS LIKE 'key_writes';

# key_blocks_unused、key_cache_block_size、key_buffer_size
# 评估key buffer 的使用率来判断索引缓存设置是否合理 1-((key_blocks_unused*key_cache_block_size)/key_buffer_size)
# 一般来说使用率在80% 左右比较合适
SHOW STATUS LIKE 'key_blocks_unused';
SHOW VARIABLES LIKE 'key_cache_block_size';
SHOW VARIABLES LIKE 'key_buffer_size';

SELECT 1 - ((6696 * 1024) / 8388608)
FROM dual; -- 0.1826

# 2-使用多个索引缓存
# hot_cache:新建索引缓存的名称
SET GLOBAL hot_cache.key_buffer_size = 128 * 1024; -- 新建

SET GLOBAL hot_cache.key_buffer_size = 0; -- 关闭

# 不能删除默认的 key_buffer_size
SHOW VARIABLES LIKE 'key_buffer_size';
SHOW VARIABLES LIKE 'hot_cache.key_buffer_size';

CACHE INDEX sakila.payment IN hot_cache;

SHOW WARNINGS ;





