# 简单实例
# 定期分析表和检查表

# 分析表的语句
# ANALYZE [LOCAL|NO_WRITE_TO_BINLOG] TABLENAME : 分析和存储表的关键字分布
ANALYZE TABLE payment, payment_myisam;
ANALYZE LOCAL TABLE payment;

# 检查表语句
# CHECK TABLE TABLENAME [QUICK|FAST|EXTENDED|CHANGED] : 检查表是否有错误。
CHECK TABLE payment_myisam QUICK FAST MEDIUM EXTENDED CHANGED;

# 定期优化表
# OPTIMIZE [LOCAL|NO_WRITE_TO_BINLOG] TABLE TABLENAME:如果已经删除了表的大部分，或者已经对含有(varchar、blob、text)的表进行了很多
# 更改，则应该使用 OPTIMIZE TABLE 命令来进行表优化。
OPTIMIZE TABLE payment_myisam;

# 对于InnoDB引擎的表来说，通过设置 INNODB_FILE_PER_TABLE 参数，设置 InnoDB 为独立表空间模式，这样每个数据库的表都会生成独立的 ibd 文件，用于
# 存储表的数据和索引，这样可以在一定程度上减轻 InnoDB 表的空间回收问题。
# 在删除大量数据后， InnoDB 表可以通过 alter table 但是不修改引擎的方式来回收不用的空间
SET INNODB_FILE_PER_TABLE := 1;
ALTER TABLE payment
  ENGINE = InnoDB;