-- 索引的问题
SELECT database();
SELECT schema();
USE sakila;

# 索引的分类
# 索引时在存储引擎中实现的，而不是在服务器层实现的。所以每种存储引擎的索引都不完全相同

# 四种索引
# B-Tree：最常见的索引类型，大部门都支持
# HASH：只有 Memory 引擎支持，使用场景简单
# R-Tree(空间索引)：空间索引是MyISAM 的一个特殊索引类型，主要用于地理空间数据类型，较少使用
# Full-text(全文索引)：MyISAM和InnoDB支持

# MySQL目前不支持函数索引，但是支持对列的前面某一部分进行索引：前缀索引（有弊端，order by 与 group by无法使用）
# 创建前缀索引
CREATE INDEX idx_dept_name_prefix
  ON learning.dept (deptname(2));

# 索引的存储




# 索引的使用
# 1-匹配全值：对索引中所有列都指定具体值，即对银锁中的所有列都有等值的匹配
# type = const(常量),key=rental_date，表示优化器选择 rental_date 进行扫描
EXPLAIN SELECT *
        FROM rental
        WHERE rental_date = '2005-05-24 22:53:30' AND inventory_id = 367 AND rental.customer_id = 130;

# 2-匹配值得范围查询。对索引的值能够进行范围查找
# type = range,范围查询；lry= idx_fk_customer_id 优化器选择 idx_fk_customer_id 来加速访问
EXPLAIN SELECT *
        FROM rental
        WHERE customer_id >= 373 AND customer_id < 400;

# 3-匹配最左前缀：仅仅使用索引中最左边列进行查询。如索引col1+col2+col3，能够被包含col1、(col1+col2)、(col1+col2+col3)等值利用
# 会利用索引
EXPLAIN SELECT *
        FROM rental
        WHERE rental_date = '2005-05-24 22:53:30';

EXPLAIN SELECT *
        FROM rental
        WHERE rental_date = '2005-05-24 22:53:30' AND inventory_id = 367;

# 无法使用索引（修改索引的列）
EXPLAIN SELECT *
        FROM rental
        WHERE inventory_id = 367;

EXPLAIN SELECT *
        FROM rental
        WHERE inventory_id = 367 AND rental.customer_id = 130;

# 4-仅仅对索引进行查询
# Extra using index ，不需要通过索引回表
EXPLAIN SELECT rental_date
        FROM rental
        WHERE rental_date = '2005-05-24 22:53:30' AND inventory_id = 367 AND rental.customer_id = 130;

# 5、匹配列前缀，仅仅使用索引中的第一列
# 6、能够实现索引匹配部分精确而其他部分进行范围匹配
EXPLAIN SELECT *
        FROM rental
        WHERE rental_date = '2005-05-24 22:53:30' AND customer_id > 30 AND rental.customer_id < 400;

# 7、如果列名是索引，那么使用column_name is null 就会使用索引
EXPLAIN SELECT *
        FROM payment
        WHERE rental_id IS NULL;

# ICP：index condition pushdown 的特性。即某种情况下条件过滤下放到存储引擎


# 存在索引，但是不能使用索引的场景
# 1- 以 % 开头的like查询不能够利用 B-Tree 索引，key=NULL，没有使用索引
# 使用索引
EXPLAIN SELECT *
        FROM actor
        WHERE last_name LIKE 'NI%';

# 没有使用索引，
EXPLAIN SELECT *
        FROM actor
        WHERE last_name LIKE '%NI%';

# 解决方案：
# 方案1：推荐使用FullText 来解决类似的全文检索问题
# 方案2：利用InnoDB 的表都是聚簇表的特点，采取一种轻量级别的解决方式：一般情况下，索引都比表小。而InnoDB 表上二级索引
# idx_last_name 实际上存储字段 last_name 还有主键 actor_id,那么理想的访问方式应该是首先扫描二级索引 idx_last_name
# 获得满足条件 last_name like '%NI%' 的主键 actor_id 列表，之后根据主键回表去检索记录，这样访问避免了全表扫描演员表
# actor 产生的大量IO 请求。

# 从执行计划中能够看到，内层查询的 Using index 代表索引覆盖扫描，之后通过主键 join 操作去演员表 actor 中获取做种查询结果
EXPLAIN SELECT *
        FROM (SELECT actor_id
              FROM actor
              WHERE last_name LIKE '%NI%') a, actor b
        WHERE a.actor_id = b.actor_id;

# 2-数据类型出现隐式转换的时候也不会使用索引，特别是当列类型是字符串，那么一定记得在 where 条件中把字符串
# 常量用引号起来，否则即便这个列表上有索引，MySQL 也不会用到，因为 MySQL 默认把输入的常量值进行转换以后才进行检索。
# 例如, 演员表actor 中的姓氏字段 last_name 是字符型的，但是 SQL 语句中的条件 1 是一个数值型值，因此即便存在索引
# idx_last_name，MySQL 也不能正确地用上索引，而是继续进行全表扫描：
# 使用索引
EXPLAIN SELECT *
        FROM actor
        WHERE last_name = '1';
# 不能使用索引
EXPLAIN SELECT *
        FROM actor
        WHERE last_name = 1;

# 3-复合索引的情况下，假如查询条件不包含索引列最左边部分，即不满足最左原则 Leftmost，是不会使用复合索引的
EXPLAIN SELECT *
        FROM payment
        WHERE amount = 3.98 AND last_update = '2006-02-15 22:12:32';

# 4-如果 MySQL 估计使用索引比全表扫描更慢，则不使用索引
UPDATE film_text
SET title = concat('S', title);

EXPLAIN SELECT *
        FROM film_text
        WHERE title LIKE 'S%';

# 5-用 or 分割开的条件，如果 or 前的条件中的列有索引，而后面的列中没有索引，那么涉及的索引都不会被用到
EXPLAIN SELECT *
        FROM payment
        WHERE customer_id = 203 OR amount = 3.96;

# 查看索引使用情况
# 如果索引正在工作，Handler_read_key 的值将很高，这个值代表了一个行被索引值读的次数。很低的值表明增加索引得到的性能改善不高，因为索引并不经常使用
# Handler_read_rnd_next 的值高则意味着查询运行低效，并且应该建立索引补救。这个值的含义是在数据文件中读下一行的请求数。如果进行大量的表扫描，
# Handler_read_rnd_next 的值较高，则通常说明表索引不正确或者写入的查询没有利用索引

# Handler_read_key 越高越好
# Handler_read_rnd_next 越低越好
SHOW STATUS LIKE 'handler_read%';



















