# SQL优化的一般步骤
USE sakila;

# 优化1、show status 了解各种SQL的执行频率

# 服务器状态信息 mysqladmin extended-status
SHOW STATUS; -- 等于 session,当前连接
SHOW SESSION STATUS;
SHOW GLOBAL STATUS;

# 当前session中所有统计参数的值
SHOW STATUS LIKE 'Com_%';
# Com_ 表示每个XXX 语句执行的次数
# Com_select：执行 Select 操作的次数，已从查询只累加1
# Com_insert：执行insert 操作的次数，对于批量插入的 insert操作，只累加一次
# Com_update：
# Com_delete：

# InnoDB存储引擎有效
SHOW STATUS LIKE 'InnoDB_rows%';
# Innodb_rows_read: select 返回的行数
# Innodb_rows_inserted：执行insert插入的行数
# Innodb_rows_updated：执行update 操作的行数
# Innodb_rows_deleted：执行delete 操作的行数

# 事务数据
SHOW STATUS LIKE 'Com_commit';
SHOW STATUS LIKE 'Com_rollback';

# 数据库基本情况
SHOW STATUS LIKE 'Connections'; -- 试图连接数据库服务器的次数
SHOW STATUS LIKE 'Uptime'; -- 服务器工作时间
SHOW STATUS LIKE 'Slow_queries'; -- 慢查询的次数


# 优化2、定位执行效率较低的sql语句
# 1、慢查询日志 启动时 --log-slow-queries[=file_name],mysqld 写一个包含所有执行时间超过 long_query_time 秒的sql语句的日志文件
# 2、通过查看线程状态
SHOW PROCESSLIST;

# 优化3、通过explain、desc 分析低效率SQL 的执行计划

DESC SELECT sum(amount)
     FROM customer AS a, payment AS b
     WHERE 1 = 1 AND a.customer_id = b.customer_id AND a.email = 'JANE.BENNETT@sakilacustomer.org';

# select_type:表示select 的类型。SIMPLE、PRIMARY、UNION、SUBQUERY
# table:输出结果集的表

# type:表示mysql在表中找到所需行的方式:ALL->index->range->ref->eq_ref->const,system->NULL(性能由低到高)
# type=ALL：全表扫描
# type=index：索引全表扫描，遍历整个索引
# type=range：索引范围扫描，常见于<、<=、>、>=、between等操作符
# type=ref:使用非唯一索引扫描或唯一索引的前缀扫描，返回匹配某个单独值得记录行
# 索引 idx_fk_customer_id 是非唯一索引，查询条件为等值查询条件 customer_id = 35。所以扫描索引类型为ref。经常出现在join中
EXPLAIN SELECT
          a.*,
          b.*
        FROM payment a, customer b
        WHERE a.customer_id = b.customer_id;

SELECT *
FROM payment
WHERE customer_id = 35;

# eq_ref:唯一索引，每个索引键值，表中只有一条记录。简单就是多表连接中使用primary key 或者 unique index 作为关联条件
EXPLAIN SELECT *
        FROM film AS a, film_text AS b
        WHERE a.film_id = b.film_id;

# type=const,system：单表中最多有一个匹配行，查询起来非常迅速，所以这个匹配行中的其他列的值可以被优化器在当前查询中当做常量来处理，例如：根据主键primary key 或者唯一索引 unique index进行查询

EXPLAIN SELECT *
        FROM (
               SELECT *
               FROM customer
               WHERE customer_id = 35) AS a;

# type=NULL ：不用访问表或者索引
EXPLAIN SELECT 1
        FROM dual;

# type=其他值。


# EXPLAIN EXTENDED  命令，show warnings 优化器做的优化
EXPLAIN EXTENDED SELECT count(amount)
                 FROM customer a, payment b
                 WHERE 1 = 1 AND a.customer_id = b.customer_id AND a.email = 'JANE.BENNETT@sakilacustomer.org';

SHOW WARNINGS;
# 优化器处理后的SQL
/* select#1 */ SELECT count(`sakila`.`b`.`amount`) AS `count(amount)`
               FROM `sakila`.`customer` `a`
                 JOIN `sakila`.`payment` `b`
               WHERE ((`sakila`.`b`.`customer_id` = `sakila`.`a`.`customer_id`) AND
                      (`sakila`.`a`.`email` = 'JANE.BENNETT@sakilacustomer.org'));

# EXPLAIN EXTENDED 支持分区

# 创建hash分区表
CREATE TABLE sakila.customer_part (
  customer_id SMALLINT(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (customer_id)
) PARTITION BY HASH (customer_id) PARTITIONS 5;

# 初始化数据
INSERT INTO sakila.customer_part SELECT customer_id
                                 FROM sakila.customer;
# 可以看出，结果来自p0,并且type=const,是根据主键查询的
EXPLAIN PARTITIONS SELECT *
                   FROM customer_part
                   WHERE customer_id = 130;

# 优化4、通过SHOW PROFILE 分析SQL。可以更清楚的了解SQL执行的过程

# 查看 MySQL是否支持profile
SELECT @@have_profiling;

# profile 是否打开，默认是关闭的
SELECT @@profiling;
SET SESSION PROFILING = 1; -- 打开

# 实例：MyISAM 表有表的元数据缓存，那么对于 MyISAM 表的COUNT(*)是不需要消耗太多的资源的。而对于 InnoDB 来说，就没有这种元数据，COUNT(*)执行的比较慢

# InnoDB 表 payment
SHOW CREATE TABLE payment;
# 先执行SQL
SELECT count(*)
FROM payment;
# 查看执行 的query id
SHOW PROFILES;

# 查看该SQL的执行过程中每个状态和消耗的时间
# Sending data 状态耗时较多，指线程开始访问数据行并把结果返回给客户端（大量的磁盘读取）
# ALL、CPU、BLOCK IO、CONTEXT
SHOW PROFILE ALL FOR QUERY 90;

# 排序结果
SELECT *
FROM information_schema.PROFILING;

SET @query_id := 126;
SELECT
  STATE,
  sum(DURATION)                                                AS Total_R,
  round(100 * sum(DURATION) / (SELECT sum(DURATION)
                               FROM information_schema.PROFILING
                               WHERE QUERY_ID = @query_id), 2) AS Calls,
  sum(DURATION) / count(*)                                     AS "R/Call"
FROM information_schema.PROFILING
WHERE QUERY_ID = @query_id
GROUP BY STATE
ORDER BY Total_R DESC;

# 进一步查看其他参数，如CPU
SHOW PROFILE CPU FOR QUERY 195;

# MyISAM表测试
CREATE TABLE payment_myisam LIKE payment;

ALTER TABLE payment_myisam
  ENGINE = myisam;

INSERT INTO payment_myisam SELECT *
                           FROM payment;

SELECT count(*)
FROM payment_myisam;

SHOW PROFILES;

SHOW PROFILE ALL FOR QUERY 254;
SHOW PROFILE SOURCE FOR QUERY 254; -- 源码文件、函数名
# 可以看到 InnoDB的表在count(*) 时经历了Sendind data 状态，而 myisam 的表在executing 之后直接结束查询了，不需要访问数据库


# 优化5、trace 分析优化器如何选择执行计划

# 打开trace 设置格式为json,设置trace 的最大能够使用的内存大小
SET OPTIMIZER_TRACE = "enabled=on", END_MARKERS_IN_JSON = ON;
SET OPTIMIZER_TRACE_MAX_MEM_SIZE = 10000000;

# 执行想做trace 的执行语句
SELECT *
FROM rental
WHERE 1 = 1 AND rental_date >= '2005-05-25 04:00:00' AND rental.rental_date <= '2005-05-25 05:00:00' AND
      rental.inventory_id = 4466;

SELECT *
FROM information_schema.OPTIMIZER_TRACE;




