# 常用SQL 优化
# 优化一、大批量插入数据
# 当用 load 命令导入数据的时候，适当的设置可以提高导入的速度

# MyISAM: DISABLE KEYS 与 ENABLE KEYS
# DISABLE KEYS 与 ENABLE KEYS 用来打开或者关闭 MyISAM 表唯一索引的更新。在导入大量数据到一个飞空的 MyISAM 表时
# 通过设置者两个命令，可以提高导入的效率。对于导入数据到空的 MyISAM 表，默认是先导入数据然后才创建索引，所以不需要

USE learning;
CREATE TABLE dept_myisam LIKE dept;
ALTER TABLE dept_myisam
  ENGINE = MyISAM;
SHOW CREATE TABLE dept_myisam;
SHOW TABLE STATUS LIKE 'dept_myisam';

# 设置参数再导入，可以提高6倍的速度
ALTER TABLE payment_myisam
  DISABLE KEYS;
LOAD DATA INFILE '/home/mysql/file_test.txt' INTO TABLE payment_myisam;
;
ALTER TABLE payment_myisam
  ENABLE KEYS;

# 直接导入
LOAD DATA INFILE '/home/mysql/file_test.txt' INTO TABLE payment_myisam;

# InnoDB 表:
# 1-因为 InnoDB 类型的表时按照主键的顺序拍保存的，所以将导入的数据按照主键的顺序排列，可以有效的提高导入的效率
# 2-导入前 执行前关闭唯一检查检验，导入后打开检查，可以提高效率
# SET UNIQUE_CHECKS = 0 ;
# SET UNIQUE_CHECKS = 1 ;
# 3-导入前关闭自动事务提交，导入后打开
# SET AUTOCOMMIT = 0;
# SET AUTOCOMMIT = 1;


# 优化二、优化 insert 语句
# 1-同一个客户端：多个值表的insert插入
INSERT INTO dept_myisam (deptno, deptname) VALUES (11, 'sam'), (22, 'tom');

# 2-不同的客户端： INSERT DELAYED ,让insert 语句马上执行，其实数据都被放在内存的队列中并没有真正写入磁盘。
# INSERT LOW_PRIORITY 刚好相反，在所有其他用户对表的读写完成后才进行插入
INSERT DELAYED dept_myisam (deptno, deptname) VALUES (11, 'sam'), (22, 'tom');
INSERT LOW_PRIORITY dept_myisam (deptno, deptname) VALUES (11, 'sam'), (22, 'tom');

# 3-将索引文件和数据文件分在不同的磁盘上存放(利用建表中选项)


# 4-批量插入 ,可以增加 bilk_insert_buffer_size 的值，对 MyISAM 有效
# 5-当从一个文件装载一个表时，使用 load data infile.这通常比使用很多inser 快20倍。


# 优化三、优化 order by 语句
USE sakila;
# 了解MySQL 的排序方式
SHOW INDEX FROM sakila.customer;

# 两种排序方式
# 1-通过有序索引顺序扫描直接返回有序数据，这种方式在使用 explain 分析查询的时候显示为 Using index,不需要额外的排序，操作效率较高
EXPLAIN SELECT customer_id
        FROM sakila.customer
        ORDER BY store_id;

# 2-通过对返回数据进行排序(Using filesort)。全表扫描。
# filesort 是通过相应的排序算法，将取得的数据在 sort_buffer_size 系统变量设置的内存排序区中进行排序。内存不够，将在在磁盘上的数据
# 分块，在对各个数据块进行排序，然后将各个块合并成有序的结果集。sort_buffer_size 设置的排序区是每个线程独占的，所以同一时刻，MySQL
# 存在多个 sort_buffer 排序区。

# 例子：按照store_id 排序返回 customer的所有数据，会全表扫描
EXPLAIN SELECT *
        FROM sakila.customer
        ORDER BY store_id;

# 例子： 只需要获取商店store_id 和 顾客的email 信息时，对表 customer 的扫描就被覆盖索引 idx_storeid_email 扫描
# 代替，此时虽然只访问了索引就足够，但是在索引 idx_storeid_email 上发生了一次排序操作，所以执行计划中仍然有
# Using filesort
ALTER TABLE sakila.customer
  ADD INDEX idx_storeid_email(store_id, email);

EXPLAIN SELECT
          store_id,
          email,
          customer_id
        FROM sakila.customer
        ORDER BY email;

# 排序优化的目标：减少额外排序，通过索引直接返回有序数据。
# WHERE和 order by 使用相同的索引，并且 order by 的顺序和索引顺序相同，并且 order by 的字段都是升序或者都是降序。
# 否则肯定需要额外的排序操作，这样就会出现 folesort

# 例子：查询商店编号为1，按照email 逆序排序的记录主键 customer_id 时，优化器使用扫描索引
EXPLAIN SELECT
          store_id,
          email,
          customer_id
        FROM sakila.customer
        WHERE store_id = 1
        ORDER BY email DESC;

# 总结：
# 使用索引
# SELECT * FROM tablename ORDER BY key_part1,key_part2 ...;
# SELECT * FROM tablename WHERE key_part1 = 1 ORDER BY key_part1 DESC ,key_part2 DESC ...;
# SELECT * FROM tablename ORDER BY key_part1 DESC ,key_part2 DESC ...;

# 不使用索引
# 1-order by 的字段混合asc 与 desc
# SELECT * FROM tablename ORDER BY key_part1 DESC ,key_part2 ASC ...;

# 2-用于查询的关键字与order by 中所用的不相同
# SELECT * FROM tablename WHERE key1 = 1 ORDER BY key2 ...;

# 3-对不同的关键字使用 ORDER BY
# SELECT * FROM tablename ORDER BY key1 ,key2 ...;


# Filesort的优化
# 1- 两次扫描算法
# 2- 一次扫描算法


# 优化四、优化 group by 语句
# 默认情况下，MySQL 对所有 GROUP BY col1, col2... 字段进行排序
# 设置非排序
EXPLAIN SELECT
          payment_date,
          sum(amount)
        FROM sakila.payment
        GROUP BY payment_date;

# 不排序，不会进行 Using filesort
EXPLAIN SELECT
          payment_date,
          sum(amount)
        FROM sakila.payment
        GROUP BY payment_date
        ORDER BY NULL;

# 优化五、优化 OR 语句
# 对于含有 OR 的查询字句，如果要利用索引，则 OR 之间的每个条件列都必须用到索引。如果没有索引，则应该考虑增加索引。
SHOW INDEX FROM sakila.store;

# 优化六、优化分页语句：创建覆盖索引能够比较好的提高性能

# 例子 limit 1000,20 这种查询
EXPLAIN SELECT
          film_id,
          description
        FROM sakila.film
        ORDER BY title
        LIMIT 50, 5;

# 优化1、尽可能少的扫描页面
EXPLAIN SELECT
          a.film_id,
          a.description
        FROM sakila.film AS a
          INNER JOIN (SELECT film_id
                      FROM sakila.film
                      ORDER BY title
                      LIMIT 50, 5) AS b ON a.film_id = b.film_id;

# 优化2、增加一个 last_page_record

# 获取第42页的数据
EXPLAIN SELECT *
        FROM sakila.payment
        ORDER BY rental_id DESC
        LIMIT 410, 10;

# 上一次查询的数据
EXPLAIN SELECT
          payment_id,
          rental_id
        FROM sakila.payment
        ORDER BY rental_id DESC
        LIMIT 400, 10;

# 这样 Using index condition
EXPLAIN SELECT *
        FROM sakila.payment
        WHERE rental_id < 15640
        ORDER BY rental_id DESC
        LIMIT 10;

# 优化七、使用 SQL 提示

# 1- SQL_BUFFER_RESULT:强制 MySQL 生成一个临时结果集。只要临时结果集生成后，所有表上的锁定均被释放。
SELECT SQL_BUFFER_RESULT *
FROM payment;

# 2- USE INDEX:建议使用某个索引
EXPLAIN SELECT count(*)
        FROM rental
        USE INDEX (rental_date);

# 3-IGNORE INDEX:忽略一个或者多个索引
EXPLAIN SELECT count(*)
        FROM rental
        IGNORE INDEX (rental_date);

# 4-FORCE INDEX:强制使用一个特定的索引
EXPLAIN SELECT count(*)
        FROM rental
        FORCE INDEX (rental_date);

# 常见sql使用
# 1- rand()：可按照随机顺序检索数据
SELECT *
FROM category
ORDER BY rand();

# 随机抽取5条数据
SELECT *
FROM category
ORDER BY rand()
LIMIT 5;

# GROUP BY .. with rollup 显示更多的分组数据
SELECT
  date_format(payment_date, '%Y-%m'),
  staff_id,
  sum(amount)
FROM payment
GROUP BY date_format(payment_date, '%Y-%m'), staff_id;

# 添加 WITH ROLLUP
SELECT
  date_format(payment_date, '%Y-%m'),
  staff_id,
  sum(amount)
FROM payment
GROUP BY date_format(payment_date, '%Y-%m'), staff_id WITH ROLLUP;


# BIT GROUP BY FUNCTIONS 做统计
# BIT_AND
# BIT_OR























