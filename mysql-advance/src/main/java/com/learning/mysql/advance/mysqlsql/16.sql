# 9 s 681 ms -> 345 ms
explain select
          IFNULL(last_month_cumu_addTALENT(agentcode, '201804', 'BASLAW001'), 0),
          a.agentcode
        from laindexinfo1 a
        WHERE indexcalno = '201804'
              and branchtype = '1'
              and indextype = '01'
              and baslawcode = 'BASLAW001'
              and agentgrade = 'A03'
              and managecom like CONCAT('8602', '%')
        limit 0, 50;


select TO_CHAR(ADD_MONTHS(TO_DATE('201804', 'yyyymm'), -1), 'yyyymm');

explain SELECT value
        FROM
          laindexinfonew a
        WHERE
          a.branchtype = '1'
          AND agentcode = '62042973'
          AND a.indexcode = 'BAS027'
          and indexcalno = '201803'
          and a.baslawcode = 'BASLAW001';

# 新增索引
# CREATE INDEX `idx_laindexinfo1_agentgrade` ON laindexinfo1 (agentgrade);
# CREATE INDEX `laindexinfonew_agentcode_index` ON laindexinfonew (agentcode);

# 修改函数动态参数
#  DECLARE indexcalno2 varchar(12);
#  select TO_CHAR(ADD_MONTHS(TO_DATE(VWAGENO, 'yyyymm'), -1), 'yyyymm') into indexcalno2;