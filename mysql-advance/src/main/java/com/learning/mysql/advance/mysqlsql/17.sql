update laindexinfo1
set T1 = IFNULL(CalManagerType(agentcode, VWAGENO, vbaslawcode), 0)
WHERE indexcalno = VWAGENO
      and branchtype = '1'
      and indextype = '01'
      and baslawcode = vbaslawcode
      and managecom like CONCAT(VMANAGECOM, '%')
      and agentgrade >= 'B01'
      and agentgrade = M_AgentGrade;



explain select CalManagerType(agentcode, '201712', 'BASLAW00')
from laindexinfo1
WHERE indexcalno = '201711'
      and branchtype = '1'
      and indextype = '01'
      and baslawcode = 'BASLAW001'
      and managecom like CONCAT('86', '%')
      and agentgrade >= 'B01';
