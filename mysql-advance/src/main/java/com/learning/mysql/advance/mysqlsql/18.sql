update laindexinfo1
set T31 = IFNULL(calGetsecYearFYC(agentcode, VWAGENO, vbaslawcode), 0)
WHERE indexcalno = VWAGENO
      and branchtype = '1'
      and indextype = '01'
      and baslawcode = vbaslawcode
      and managecom like CONCAT(VMANAGECOM, '%')
      and agentgrade = M_AgentGrade;


explain select
          calGetsecYearFYC(agentcode, '201709', 'BASLAW001'),
          agentcode
        from laindexinfo1
        WHERE indexcalno = '201709'
              and branchtype = '1'
              and indextype = '01'
              and baslawcode = 'BASLAW001'
              and managecom like CONCAT('86', '%');


select calGetsecYearFYC('62008956', '201709', 'BASLAW001');


explain select (case
        when ((select count(*)
               from latree a, ladimission b
               where a.agentcode = b.agentcode
                     and a.agentgrade = 'A02'
                     and b.departrsn = '11'
                     and a.agentcode = '62008956'
                     and a.baslawcode = 'BASLAW001') <= 0)
          then
            'N'
        else
          'Y'
        end)
from dual;
