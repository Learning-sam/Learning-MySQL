update laindexinfo1
set T50 = IFNULL(calNewManagerAdd(agentcode, VWAGENO, vbaslawcode), 0)
WHERE indexcalno = VWAGENO
      and branchtype = '1'
      and indextype = '01'
      and baslawcode = vbaslawcode
      and managecom like CONCAT(VMANAGECOM, '%')
      and agentgrade >= 'A02'
      and agentgrade = M_AgentGrade;

explain select
          calNewManagerAdd(agentcode, '201705', 'BASLAW001'),
          agentcode
        from laindexinfo1
        WHERE indexcalno = '201705'
              and branchtype = '1'
              and indextype = '01'
              and baslawcode = 'BASLAW001'
              and managecom like CONCAT('86', '%')
              and agentgrade >= 'A02';


explain SELECT agentcode
        FROM latree D
        WHERE D.BRANCHTYPE = '1'
              AND D.INTROAGENCY = '62008956'
              AND D.INITGRADE = 'A01'
              AND NOT EXISTS(SELECT 1 # 二次入司不计算合格增员
                             FROM laagent C
                             WHERE C.AGENTCODE = D.AGENTCODE
                                   AND C.AGENTSTATE = '02')
              and BASLAWCODE = 'BASLAW001';

explain SELECT ifnull(SUM(f.STANDPREM1), 0)
from (

       SELECT ifnull(a.STANDPREM1, 0) STANDPREM1
       FROM lacommision a
       WHERE
         (a.branchtype = '1') and ((a.assessvestwage <> 'Y') or isnull(a.assessvestwage))
         and a.AGENTCODE = '62008956'
         AND a.WAGENO <= '201705'
#          AND a.WAGENO >= to_char(V_EMPLOYDATE, 'yyyymm')
         AND a.BRANCHTYPE = '1'
         AND a.PAYYEAR = '0'
         AND a.COMMDIRE = '1'
         and a.BASLAWCODE = 'BASLAW001'
         AND (a.ISDIRECTMARKET <> 'Y' OR a.ISDIRECTMARKET IS NULL)
       union
       SELECT ifnull(b.STANDPREM1, 0) STANDPREM1
       from lareplacecont b
       where b.branchtype = '1'
             and b.AGENTCODE = '62008956'
             AND b.WAGENO <= '201705'
#              AND b.WAGENO >= to_char(V_EMPLOYDATE, 'yyyymm')
             and b.BASLAWCODE = 'BASLAW001'
             AND (b.ISDIRECTMARKET <> 'Y' OR b.ISDIRECTMARKET IS NULL)


     ) f;


# 修改后的 function
SELECT ifnull(SUM(D.STANDPREM1), 0)
INTO V_STANDPREM11
FROM view_lacommision_replace_sp1 D
WHERE D.AGENTCODE = V_ROW
      AND D.WAGENO <= VWAGENO AND D.WAGENO >= to_char(V_EMPLOYDATE, 'yyyymm')
      AND D.BRANCHTYPE = '1'
      AND D.PAYYEAR = '0'
      AND D.COMMDIRE = '1'
      and BASLAWCODE = VBASLAWCODE
      AND (D.ISDIRECTMARKET <> 'Y' OR D.ISDIRECTMARKET IS NULL); # 去除挂单件*/


drop function if exists CalNewManagerAdd;


# create function CalNewManagerAdd(VAGENTCODE varchar(10), VWAGENO varchar(10), VBASLAWCODE varchar(20))
#   returns decimal
#   BEGIN
#     /**
#       * 循环判断主管对应A01增员是否在当月为合格增员 add by lixg for CS394_5 201703
#       * @param VAGENTCODE 主管员工编号
#       * @param VWAGENO 薪资计算月份
#       * @return RESULT 合格增员人数
#      */
#     DECLARE RESULT decimal(10, 0);
#     DECLARE V_EMPLOYDATE DATE; # 入司日期
#     DECLARE V_STANDPREM11 decimal(16, 6) DEFAULT 0; # 当月标保
#     DECLARE V_STANDPREM12 decimal(16, 6) DEFAULT 0; # 上月标保
#     DECLARE V_AGENTGRADE VARCHAR(5); # 当前职级;
#     DECLARE V_AssessMonths decimal DEFAULT 0; # 考核月数
#     DECLARE V_CalMonths decimal DEFAULT 0; # 入司月数
#     DECLARE V_Flag decimal DEFAULT 0; # 默认 0 以考核月计，其他为 1 以自然月计
#     DECLARE V_ROW VARCHAR(10);
#     DECLARE state int default false; # 定义表示用于判断游标是否溢出
#     DECLARE
#       V_CURSOR CURSOR FOR (
#       SELECT agentcode
#       FROM latree D
#       WHERE D.BRANCHTYPE = '1'
#             AND D.INTROAGENCY = VAGENTCODE
#             AND D.INITGRADE = 'A01'
#             /*AND EXISTS (SELECT 1 #在东吴考证
#              FROM LAQUALIFICATION B
#             WHERE B.AGENTCODE = D.AGENTCODE
#               AND B.IDX = '1'
#               AND B.EXAMCAPITALFLAG = 'Y')*/
#             AND NOT EXISTS(SELECT 1 # 二次入司不计算合格增员
#                            FROM laagent C
#                            WHERE C.AGENTCODE = D.AGENTCODE
#                                  AND C.AGENTSTATE = '02')
#             and BASLAWCODE = VBASLAWCODE);
#
#     # 将结束标志绑定到游标
#     DECLARE CONTINUE HANDLER FOR NOT FOUND SET state = TRUE;
#     /*CS415-主管个人津贴优化
#     1.去除被增人员东吴考证要求；
#     2.聘才正式定级次月合格增员按自然月判断，其他情况按考核月判断
#     */
#     #PRAGMA AUTONOMOUS_TRANSACTION;
#
#     /*如果当前职级小于A02，直接返回0.（由于主管做了特殊保护后，是按照虚拟职级计算的，
#     为了保证B01过了特殊保护后继续享受新渠道经理津贴，因此A02也会配置此指标）
#     如果是正常A01则直接返回0，不做后续逻辑判断*/
#     SELECT AGENTGRADE
#     INTO V_AGENTGRADE
#     FROM latree
#     WHERE AGENTCODE = VAGENTCODE
#           and BASLAWCODE = VBASLAWCODE;
#     IF V_AGENTGRADE < 'A02'
#     THEN
#       RETURN 0;
#     END IF;
#
#     # 薪资计算月是聘才主管正式定级次月
#     SELECT count(1)
#     into V_Flag
#     from latree a
#     WHERE a.agentcode = VAGENTCODE
#           AND a.speciflag = 'Y'
#           and a.insideflag = '2'
#           and BASLAWCODE = VBASLAWCODE
#           and (a.agentkind is null or a.agentkind <> 'Y')
#           and to_char(add_months(a.uwmodifydate, 1), 'YYYYMM') = VWAGENO;
#
#     # CS426 正常晋升的主管，晋升当月合格人力按自然月计算
#     if V_Flag = 0
#     then
#       SELECT count(1)
#       into V_Flag
#       from laindexinfo1 b
#       WHERE b.agentcode = VAGENTCODE
#             AND b.t1 = 1
#             AND b.k1 = VWAGENO
#             AND b.indexcalno = VWAGENO
#             and BASLAWCODE = VBASLAWCODE;
#     end if;
#
#     /*计算合格增员人数。
#     合格增员：在东吴考证并且入司当月标保≥3000*/
#
#     BEGIN
#
#       OPEN V_CURSOR;
#       count_loop: LOOP
#         fetch V_CURSOR
#         into V_ROW;
#         if state
#         then
#           leave count_loop;
#         ELSE
#           BEGIN
#             # 获取考核月数，若为1则继续计算# 获得入司日期
#             SELECT
#               D.EMPLOYDATE,
#               T35,
#               T13
#             INTO V_EMPLOYDATE, V_CalMonths, V_AssessMonths
#             FROM laindexinfo1 D
#             WHERE D.AGENTCODE = V_ROW
#                   and indexcalno = VWAGENO
#                   and baslawcode = VBASLAWCODE;
#             IF isnull(V_AssessMonths)
#             THEN
#               SET V_AssessMonths = 0;
#             END IF;
#           END;
#           # 考核月数为1
#           IF V_AssessMonths = 1 and V_Flag = 0
#           THEN
#             #获取入司至今标保（一般为当月标保，上月16号及之后入司的为这两月标保；标保达3000则满足人力判断）
#             /*SELECT ifnull(SUM(D.STANDPREM1), 0)
#             INTO V_STANDPREM11
#             FROM view_lacommision_replace_sp1 D
#             WHERE D.AGENTCODE = V_ROW
#                   AND D.WAGENO <= VWAGENO
#                   AND D.WAGENO >= to_char(V_EMPLOYDATE, 'yyyymm')
#                   AND D.BRANCHTYPE = '1'
#                   AND D.PAYYEAR = '0'
#                   AND D.COMMDIRE = '1'
#                   and BASLAWCODE = VBASLAWCODE
#                   AND (D.ISDIRECTMARKET <> 'Y' OR D.ISDIRECTMARKET IS NULL); # 去除挂单件*/
#
#             SELECT ifnull(SUM(f.STANDPREM1), 0)
#             INTO V_STANDPREM11
#             from (
#                    SELECT ifnull(a.STANDPREM1, 0) as STANDPREM1
#                    FROM lacommision a
#                    WHERE
#                      (a.branchtype = '1') and ((a.assessvestwage <> 'Y') or isnull(a.assessvestwage))
#                      and a.AGENTCODE = V_ROW
#                      AND a.WAGENO <= VWAGENO
#                      AND a.WAGENO >= to_char(V_EMPLOYDATE, 'yyyymm')
#                      AND a.BRANCHTYPE = '1'
#                      AND a.PAYYEAR = '0'
#                      AND a.COMMDIRE = '1'
#                      and a.BASLAWCODE = VBASLAWCODE
#                      AND (a.ISDIRECTMARKET <> 'Y' OR a.ISDIRECTMARKET IS NULL)
#                    union
#                    SELECT ifnull(b.STANDPREM1, 0) as STANDPREM1
#                    from lareplacecont b
#                    where b.branchtype = '1'
#                          and b.AGENTCODE = V_ROW
#                          AND b.WAGENO <= VWAGENO
#                          AND b.WAGENO >= to_char(V_EMPLOYDATE, 'yyyymm')
#                          and b.BASLAWCODE = VBASLAWCODE
#                          AND (b.ISDIRECTMARKET <> 'Y' OR b.ISDIRECTMARKET IS NULL)) f;
#
#             IF V_STANDPREM11 >= 3000
#             THEN
#               SET RESULT = RESULT + 1;
#             END IF;
#           ELSEIF V_Flag = 1 and V_CalMonths = 1
#             then
#
#               /*SELECT ifnull(SUM(D.STANDPREM1), 0)
#               INTO V_STANDPREM11
#               FROM view_lacommision_replace_sp1 D
#               WHERE D.AGENTCODE = V_ROW
#                     AND D.WAGENO = VWAGENO
#                     AND D.BRANCHTYPE = '1'
#                     AND D.PAYYEAR = '0'
#                     AND D.COMMDIRE = '1'
#                     and BASLAWCODE = VBASLAWCODE
#                     AND (D.ISDIRECTMARKET <> 'Y' OR D.ISDIRECTMARKET IS NULL); # 去除挂单件*/
#
#               SELECT ifnull(SUM(f.STANDPREM1), 0)
#               INTO V_STANDPREM11
#               from (
#                      SELECT ifnull(a.STANDPREM1, 0) as STANDPREM1
#                      FROM lacommision a
#                      WHERE
#                        (a.branchtype = '1') and ((a.assessvestwage <> 'Y') or isnull(a.assessvestwage))
#                        and a.AGENTCODE = V_ROW
#                        AND a.WAGENO = VWAGENO
#                        AND a.BRANCHTYPE = '1'
#                        AND a.PAYYEAR = '0'
#                        AND a.COMMDIRE = '1'
#                        and a.BASLAWCODE = VBASLAWCODE
#                        AND (a.ISDIRECTMARKET <> 'Y' OR a.ISDIRECTMARKET IS NULL)
#                      union
#                      SELECT ifnull(b.STANDPREM1, 0) as STANDPREM1
#                      from lareplacecont b
#                      where b.branchtype = '1'
#                            and b.AGENTCODE = V_ROW
#                            AND b.WAGENO = VWAGENO
#                            and b.BASLAWCODE = VBASLAWCODE
#                            AND (b.ISDIRECTMARKET <> 'Y' OR b.ISDIRECTMARKET IS NULL)) f;
#
#               IF V_STANDPREM11 >= 3000
#               THEN
#                 SET RESULT = RESULT + 1;
#               END IF;
#           END IF;
#         END IF;
#
#       END LOOP;
#       CLOSE V_CURSOR;
#     END;
#
#     RETURN (RESULT);
#   END;






