update laindexinfo1
set T3 = IFNULL(calDepartmentManageBonusRears(agentcode, VWAGENO, vbaslawcode), 0)
WHERE indexcalno = VWAGENO
      and branchtype = '1'
      and indextype = '01'
      and baslawcode = vbaslawcode
      and managecom like CONCAT(VMANAGECOM, '%')
      and agentgrade >= 'C01'
      and agentgrade = M_AgentGrade;


explain select
          calDepartmentManageBonusRears(agentcode, '201705', 'BASLAW001'),
          agentcode
        from laindexinfo1
        WHERE indexcalno = '201705'
              and branchtype = '1'
              and indextype = '01'
              and baslawcode = 'BASLAW001'
              and managecom like CONCAT('86', '%')
              and agentgrade >= 'C01';


explain SELECT IFNULL(COUNT(1), 0)
FROM larearrelation R
WHERE R.REARAGENTCODE = '62000010'
      AND R.REARLEVEL = '01'
      AND R.REAREDGENS = '1'
      AND R.REARFLAG = '1'
      and BaslawCode = 'BASLAW001'
      AND R.STARTDATE BETWEEN ADD_MONTHS(TO_DATE('201705', 'YYYYMM'), -23) AND
      LAST_DAY(TO_DATE('201705', 'YYYYMM'))
      AND (ISNULL(R.BASERELATIONFLAG) OR R.BASERELATIONFLAG != 'Y') # 被育成组不能是基础组、二次晋升的组
      AND EXISTS(SELECT 1
                 FROM laagent
                 WHERE AGENTCODE = R.AGENTCODE
                       AND AGENTSTATE IN ('01', '02')) # 计算薪资时，被育成人需处于在职状态，才算为一个有效的直接育成组
      AND NOT EXISTS(SELECT 1 # 被育成人不能是聘才筹备期 edit by wangnh 改为被育成人不能是聘才
                     FROM latree
                     WHERE SPECIFLAG = 'Y'
                           and BaslawCode = 'BASLAW001'
                           AND AGENTCODE = R.AGENTCODE)
      AND NOT EXISTS
(SELECT 1 # 新基本法改造,聘才人员不论职级变化都不享受增减比例
 FROM latree
 WHERE SPECIFLAG = 'N'
       and BaslawCode = 'BASLAW001'
       AND (CONNSUCCDATE2 IS NOT NULL OR CONNSUCCDATE2 <> '')
       AND AGENTCODE = R.AGENTCODE)
      AND (ISNULL(R.ENDDATE) OR
           ENDDATE > LAST_DAY(TO_DATE('201705', 'YYYYMM')));



