update laindexinfo1
set T48 = IFNULL(calfistpromoterearrate(agentcode, VWAGENO, vbaslawcode), 0)
WHERE indexcalno = VWAGENO
      and branchtype = '1'
      and indextype = '01'
      and baslawcode = vbaslawcode
      and managecom like CONCAT(VMANAGECOM, '%')
      and agentgrade >= 'B01'
      and agentgrade = M_AgentGrade;


explain select
          calfistpromoterearrate(agentcode, '201712', 'BASLAW001'),
          agentcode
        from laindexinfo1
        WHERE indexcalno = '201712'
              and branchtype = '1'
              and indextype = '01'
              and baslawcode = 'BASLAW001'
              and managecom like CONCAT('86', '%')
              and agentgrade >= 'B01';

# INTO VSPECIFLAG, VINSIDEFLAG
SELECT
  SPECIFLAG,
  INSIDEFLAG
FROM latree
WHERE BRANCHTYPE = '1'
      AND AGENTCODE = '62000010'
      AND baslawcode = 'BASLAW001';


explain SELECT ifnull(COUNT(1), 0)
FROM larearrelation R
WHERE R.REARAGENTCODE = '62000010'
      AND R.REARLEVEL = '01'
      AND R.REAREDGENS = '1'
      AND R.REARFLAG = '1'
      AND baslawcode = 'BASLAW001'
      AND R.STARTDATE BETWEEN ADD_MONTHS(TO_DATE('201712', 'YYYYMM'), -23) AND LAST_DAY(TO_DATE('201712', 'YYYYMM'))
      #AND startDate > v_connDate
      AND (R.BASERELATIONFLAG IS NULL OR R.BASERELATIONFLAG != 'Y') # 被育成组不能是基础组
      AND EXISTS(SELECT 1
                 FROM laagent
                 WHERE AGENTCODE = R.AGENTCODE
                       AND AGENTSTATE IN ('01', '02'))
      AND NOT EXISTS(SELECT 1
                     FROM latree
                     WHERE SPECIFLAG = 'Y'
                           #AND insideFlag = '1'
                           AND AGENTCODE = R.AGENTCODE
                           AND baslawcode = 'BASLAW001')
      AND NOT EXISTS
(SELECT 1 #新基本法改造,聘才人员不论职级变化都不享受增减比例
 FROM latree
 WHERE SPECIFLAG = 'N'
       AND CONNSUCCDATE2 IS NOT NULL
       AND AGENTCODE = R.AGENTCODE
       AND baslawcode = 'BASLAW001')
      AND (R.ENDDATE IS NULL OR
           ENDDATE > LAST_DAY(TO_DATE('201712', 'YYYYMM')))
      AND exists(select 1
                 from laindexinfo1
                 where agentcode = R.AGENTCODE and indexcalno = '201712' and baslawcode = 'BASLAW001'
                       and (T32 >= 0.75 or T32 = -1))
      #被育成人继续率大于等于0.75
      AND exists(select 1
                 from laindexinfo1
                 where agentcode = R.AGENTCODE and indexcalno = '201712' and baslawcode = 'BASLAW001'
                       and (T20 >= 0.75 or T20 = -1));



