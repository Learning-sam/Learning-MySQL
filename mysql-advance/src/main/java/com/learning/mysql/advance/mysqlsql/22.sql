update laindexinfo1
set T36 = IFNULL(caltraninpersp(agentcode, VWAGENO, vbaslawcode), 0)
WHERE indexcalno = VWAGENO
      and branchtype = '1'
      and indextype = '01'
      and baslawcode = vbaslawcode
      and managecom like CONCAT(VMANAGECOM, '%')
      and T35 <= 12
      and agentgrade = M_AgentGrade;

explain select
          caltraninpersp(agentcode, '201712', 'BASLAW001'),
          agentcode
        from laindexinfo1


        WHERE indexcalno = '201712'
              and branchtype = '1'
              and indextype = '01'
              and baslawcode = 'BASLAW001'
              and managecom like CONCAT('86', '%')
              and T35 <= 12;


SELECT IFNULL((SELECT 'Y'
               FROM latree
               WHERE AGENTCODE = '62040087'
                     AND baslawcode = 'BASLAW001'
                     AND SPECIFLAG = 'Y'
                     AND INSIDEFLAG = '1'
                     AND BRANCHTYPE = '1'),
              'N')
FROM DUAL;


select ifnull((select 'Y'
               from latree
               where agentcode = '62040087' and baslawcode = 'BASLAW001' and initgrade = 'A01'), 'N')
from dual;

# into VAGENTSTATE, V_EMPLOYDATE
select
  (case
   when a.outworkdate is null
     then #在职状态未01
       a.agentstate
   when to_char(a.outworkdate, 'YYYYMM') > '201712'
     then #离职日期大于计算年月，即计算年月还在职为01
       de_code(a.agentstate, '03', '01', '04')
   else
     '03'
   end),
  EMPLOYDATE

from laagent a
where a.AGENTCODE = '62040087';


explain select IFNULL(sum(standprem1), 0)
        from v_trainper A
        where agentcode = '62040087'
              AND (A.F5 <> 'Y' or A.F5 is null) #去除拆单件  ??拆单件是否要剔除
              AND (isdirectmarket <> 'Y' or isdirectmarket is null) #去除挂单件
              AND (ASSESSVESTWAGE <> 'Y' or ASSESSVESTWAGE is null) #“回执录入日期” 在承保月次月15号之后录入的保单,不参与当月各类津贴的计提
              and commdire = '1'
              and payyear = '0'
              and branchtype = '1'
              AND A.baslawcode = 'BASLAW001';


explain SELECT IFNULL(sum(standprem1), 0)
from (SELECT
        (case
         when abs(standprem1) = b.oldstandprem1
           then
             oldcontno
         when abs(standprem1) = b.newstandprem1
           then
             newcontno
         else
           newcontno
         end) contno,
        b.standprem1
      from lareplacecont b
      where b.agentcode = '62040087'
            AND b.baslawcode = 'BASLAW001'
            ) sql1
WHERE sql1.contno not in (SELECT sql2.contno
                          from (select distinct contno
                                from v_trainper A
                                where agentcode = 'BASLAW001'
                                      AND A.baslawcode = 'BASLAW001'

                                      AND (A.F5 = 'Y' #拆单件 ??拆单件是否要剔除
                                           or isdirectmarket = 'Y' #挂单件
                                           or ASSESSVESTWAGE = 'Y') #“回执录入日期” 在承保月次月15号之后录入的保单,不参与当月各类津贴的计提
                                      and commdire = '1'
                                      and payyear = '0'
                                      and branchtype = '1') sql2)

# 优化
# 视图（v_trainper）取消会更快，因为 agentcode 索引。






