select calmanagerpnum2('63002849', '201711', 'BASLAW001');

# 主要是 laindexinfo1 表的索引
update laindexinfo1
set T45 = IFNULL(calmanagerpnum2(agentcode, VWAGENO, vbaslawcode), 0)
WHERE indexcalno = VWAGENO
      and branchtype = '1'
      and indextype = '01'
      and baslawcode = vbaslawcode
      and managecom like CONCAT(VMANAGECOM, '%')
      and agentgrade >= 'B01'
      and agentgrade = M_AgentGrade;


explain select
          calmanagerpnum2(agentcode, '201711', 'BASLAW001'),
          agentcode
        from laindexinfo1
        WHERE indexcalno = '201711'
              and branchtype = '1'
              and indextype = '01'
              and baslawcode = 'BASLAW001'
              and managecom like CONCAT('86', '%')
              and agentgrade >= 'B01';


explain SELECT
          T43,
          IFNULL(K1, '201711')
        from laindexinfo1
        WHERE branchtype = '1'
              AND indextype = '01'
              AND indexcalno = '201711'
              AND agentcode = '62008956'
              and BASLAWCODE = 'BASLAW001';


explain SELECT IFNULL(sum(T50), 0)
        from laindexinfo1
        WHERE branchtype = '1'
              AND indextype = '01'
              AND agentcode = '62008956'
              and BASLAWCODE = 'BASLAW001';
#       and indexcalno >= '201711'
#       AND indexcalno < '201711'
#       and indexcalno between '201711' and '201711';

# 函数内部顺序调整