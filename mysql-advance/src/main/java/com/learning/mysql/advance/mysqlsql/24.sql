update laindexinfo1
set T41 = IFNULL(calFistPromoteRearBonus(agentcode, VWAGENO, vbaslawcode), 0)
WHERE indexcalno = VWAGENO
      and branchtype = '1'
      and indextype = '01'
      and baslawcode = vbaslawcode
      and managecom like CONCAT(VMANAGECOM, '%')
      and agentgrade >= 'B01'
      and agentgrade = M_AgentGrade;


explain select
          calFistPromoteRearBonus(agentcode, '201705', 'BASLAW001'),
          agentcode
        from laindexinfo1
        WHERE indexcalno = '201705'
              and branchtype = '1'
              and indextype = '01'
              and baslawcode = 'BASLAW001'
              and managecom like CONCAT('86', '%')
              and agentgrade >= 'B01';


explain select calFistPromoteRearBonus('63002849', '201705', 'BASLAW001');


explain SELECT r.agentcode
        FROM larearrelation r, laagent a, latree t
        WHERE a.agentcode = r.agentcode
              and t.agentcode = r.agentcode
              AND r.baslawcode = 'BASLAW001'
              AND t.baslawcode = 'BASLAW001'
              and t.connsuccdate2 is null
              and t.speciflag = 'N'
              and a.agentstate < '03'
              and r.REARLEVEL = '01'
              AND r.REAREDGENS = '1'
              and r.enddate is null
              AND r.REARFLAG = 1
              AND r.REARAGENTCODE = '63002849'
              AND TO_CHAR(r.STARTDATE, 'yyyymm') = '201705'
              and not exists(select 1
                             from laindexinfo1
                             where agentcode = r.agentcode
                                   AND baslawcode = 'BASLAW001'
                                   and indexcalno < '201705'
                                   and agentgrade >= 'B01');


explain SELECT r.agentcode
        FROM larearrelation r, laagent a, latree t
        WHERE a.agentcode = r.agentcode
              and t.agentcode = r.agentcode
              AND r.baslawcode = 'BASLAW001'
              AND t.baslawcode = 'BASLAW001'
              and t.connsuccdate2 is not null
              and t.speciflag = 'Y'
              and t.insideflag = '2'
              and a.agentstate < '03'
              and r.REARLEVEL = '01'
              AND r.REAREDGENS = '1'
              and r.enddate is null
              AND r.REARFLAG = 1
              AND r.REARAGENTCODE = '63002849'
              AND TO_CHAR(add_months(t.specistartdate, 2), 'yyyymm') = '201705'
              and IFNULL((select agentgrade
                          from laindexinfo1
                          where agentcode = t.agentcode
                                AND baslawcode = 'BASLAW001'
                                and indexcalno = to_char(t.connsuccdate2, 'YYYYMM')),
                         agentgrade) < 'C01'
              and to_char(t.connsuccdate2, 'YYYYMM') < '2014';


select to_char(add_months(to_date('201705', 'YYYYMM'), -1), 'yyyymm');
select to_char(add_months(to_date('201705', 'YYYYMM'), -1), 'yyyymm');

explain SELECT b.agentcode
        FROM laagent a, larearrelation b, latree t, laassessaccessory s
        WHERE a.AGENTCODE = b.AGENTCODE
              and t.agentcode = b.agentcode
              and s.agentcode = b.agentcode
              AND b.baslawcode = 'BASLAW001'
              AND t.baslawcode = 'BASLAW001'
              AND s.baslawcode = 'BASLAW001'
              AND a.employdate <= DATE('2016-3-25') #2016年3月25日（含）前签约上岗
              and t.connsuccdate2 is not null #定级日期不为空
              and t.speciflag = 'Y' #聘才
              and t.insideflag = '2' #运营期
              and a.agentstate < '03' #在职
              and b.REARLEVEL = '01' #育成级别 组育成人
              AND b.REAREDGENS = '1' #育成代目数
              and b.enddate is null #育成止期为空
              AND b.REARFLAG = 1 #育成有效
              AND b.REARAGENTCODE = '63002849' #育成人
              and IFNULL((select substr(agentgrade, 0, 1)
                          from laindexinfo1
                          where agentcode = t.agentcode
                                AND baslawcode = 'BASLAW001'
                                and indexcalno = to_char(t.connsuccdate2, 'YYYYMM')),
                         substr(t.agentgrade, 0, 1)) = 'B' #新晋升为渠道经理
              and IFNULL((select agentgrade
                          from laindexinfo1
                          where agentcode = t.agentcode
                                AND baslawcode = 'BASLAW001'
                                and indexcalno = '201705'),
                         t.agentgrade) >= 'B01' #发放时,职级在渠道经理及以上
              AND s.AGENTGRADE >= 'B01' #原职级在B01以上
              AND s.AGENTGRADE1 >= 'B01' #新职级在B01以上
              AND s.indexcalno = '201704'
              /* AND s.INDEXCALNO =(SELECT MIN(INDEXCALNO)
              FROM LAAssessAccessory
               WHERE AGENTGRADE >= 'B01'
                 AND AGENTCODE = B.AGENTCODE) --首次考核*/
              AND NOT EXISTS
        (SELECT 1
         FROM laassessaccessory
         WHERE INDEXCALNO < '201704'
               AND ASSESSTYPE = '00'
               AND baslawcode = 'BASLAW001'
               AND AGENTCODE = s.AGENTCODE
               AND BRANCHTYPE = '1'
               AND BRANCHTYPE2 = '01') #首次考核
              and to_char(t.connsuccdate2, 'YYYYMM') >= '201401';

# 添加索引
# CREATE INDEX `laassessaccessory_agentcode_index` ON laassessaccessory (agentcode);

# 修改函数动态参数写法
#     DECLARE preMonth varchar(10);
#     select to_char(add_months(to_date('201705', 'YYYYMM'), -1), 'yyyymm')
#     into preMonth;


