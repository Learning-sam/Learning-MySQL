update laindexinfo1
set K1 = IFNULL(CalManagerMonths(agentcode, VWAGENO, vbaslawcode), '')
WHERE indexcalno = VWAGENO
      and branchtype = '1'
      and indextype = '01'
      and baslawcode = vbaslawcode
      and managecom like CONCAT(VMANAGECOM, '%')
      and agentgrade >= 'B01'
      and agentgrade = M_AgentGrade;

-- 1 s 401 ms
explain select CalManagerMonths(agentcode, '201705', 'BASLAW001'),agentcode
        from laindexinfo1
        WHERE indexcalno = '201705'
              and branchtype = '1'
              and indextype = '01'
              and baslawcode = 'BASLAW001'
              and agentgrade >= 'B01'
              and managecom like CONCAT('86', '%');


# into VManagerType
explain SELECT IFNULL(T1, 0)
from laindexinfo1
WHERE indexcalno = '201705'
      and branchtype = '1'
      and indextype = '01'
      and BaslawCode = 'BASLAW001'
      and agentcode = '63006729';







