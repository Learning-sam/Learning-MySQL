update laindexinfo1
set T44 = IFNULL(calmanagerpnum1(agentcode, VWAGENO, vbaslawcode), 0)
WHERE indexcalno = VWAGENO
      and branchtype = '1'
      and indextype = '01'
      and baslawcode = vbaslawcode
      and managecom like CONCAT(VMANAGECOM, '%')
      and agentgrade >= 'B01'
      and agentgrade = M_AgentGrade;


explain select
          calmanagerpnum1(agentcode, '201711', 'BASLAW001'),
          agentcode
        from laindexinfo1
        WHERE indexcalno = '201711'
              and branchtype = '1'
              and indextype = '01'
              and baslawcode = 'BASLAW001'
              and agentgrade >= 'B01'
              and managecom like CONCAT('86', '%')
        limit 1, 500;

# INTO VMark , V_StartWageNo
SELECT
  T43,
  IFNULL(K1, '201711')
from laindexinfo1
WHERE branchtype = '1'
      AND indextype = '01'
      AND indexcalno = '201711'
      AND agentcode = '62000010'
      and BASLAWCODE = 'BASLAW001';

# into VWAGENO_1
SELECT to_char(add_months(to_date('201711', 'YYYYMM'), -3), 'YYYYMM')
from dual;

explain SELECT IFNULL(sum(T50), 0)
        from laindexinfo1
        WHERE branchtype = '1'
              AND indextype = '01'
              AND agentcode = '62000010'
              and BASLAWCODE = 'BASLAW001'
              AND indexcalno < '201711'
              and indexcalno >= '201406'
              and indexcalno between '201708' and '201711';

# 添加索引
# idx_laindexinfo1_agentcode
