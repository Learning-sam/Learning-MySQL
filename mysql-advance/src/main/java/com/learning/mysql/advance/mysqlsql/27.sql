## SQL 调优

# 27
update laindexinfo1
set T17 = IFNULL(calADDTALENT_sumfyc(agentcode, VWAGENO, vbaslawcode), 0)
WHERE indexcalno = VWAGENO
      and branchtype = '1'
      and indextype = '01'
      and baslawcode = vbaslawcode
      and managecom like CONCAT(VMANAGECOM, '%')
      and T13 <= 6
      and agentgrade = M_AgentGrade;

--
explain select
          calADDTALENT_sumfyc(agentcode, '201712', 'BASLAW001'),
          agentcode
        from laindexinfo1
        WHERE indexcalno = '201712'
              and branchtype = '1'
              and indextype = '01'
              and baslawcode = 'BASLAW001'
              and managecom like CONCAT('86', '%')
              and T13 <= 6;

-- 182099
select count(*)
from laindexinfo1;

# 31248 | 948 ms
explain select *
        from laindexinfo1
        WHERE indexcalno = '201712'
              and baslawcode = 'BASLAW001'
              and branchtype = '1'
              and indextype = '01'
              and managecom like CONCAT('86', '%')
              and T13 <= 6;

#

# 122 ms
select calADDTALENT_sumfyc('63023607', '201712', 'BASLAW001');

explain SELECT to_char(d.employdate, 'YYYYMM')
        FROM
          laagent d
        WHERE d.branchtype = '1'
              AND d.agentcode = '63023607';


explain SELECT T13
        FROM
          laindexinfo1
        WHERE branchtype = '1'
              AND agentcode = '63023607'
              AND indexcalno = '201712'
              AND BaslawCode = 'BASLAW001'
              AND indextype = '01';

explain SELECT IFNULL(SUM(T.standprem1), 0)
        FROM
          lacommision_a02_gdj_sp1 T
        WHERE AGENTCODE = '63023607'
              AND COMMDIRE = '1'
              AND PAYYEAR = '0'
              AND BRANCHTYPE = '1'
              and baslawcode = 'BASLAW001'
              AND T.WAGENO <= '201712'
              AND T.WAGENO >= '201603';

# 最终定位到视图
explain select
          `cmspdat`.`lareplacecont`.`agentcode`      AS `agentcode`,
          `cmspdat`.`lareplacecont`.`wageno`         AS `wageno`,
          `cmspdat`.`lareplacecont`.`fyc`            AS `fyc`,
          `cmspdat`.`lareplacecont`.`standprem1`     AS `standprem1`,
          `cmspdat`.`lareplacecont`.`barwertprem`    AS `duymoney`,
          '1'                                        AS `commdire`,
          0                                          AS `payyear`,
          `cmspdat`.`lareplacecont`.`branchtype`     AS `branchtype`,
          '3'                                        AS `RISKCHNL`,
          0                                          AS `p6`,
          `cmspdat`.`lareplacecont`.`branchcode`     AS `BRANCHCODE`,
          `cmspdat`.`lareplacecont`.`agentgroup`     AS `agentgroup`,
          `cmspdat`.`lareplacecont`.`branchseries`   AS `branchseries`,
          `cmspdat`.`lareplacecont`.`isdirectmarket` AS `isdirectmarket`,
          '2'                                        AS `flag`,
          `cmspdat`.`lareplacecont`.`baslawcode`     AS `baslawcode`
        from `cmspdat`.`lareplacecont`
        where ((`cmspdat`.`lareplacecont`.`branchtype` = '1') and (`cmspdat`.`lareplacecont`.`wageno` >= '201705'));


explain select
          `cmspdat`.`lacommision`.`agentcode`      AS `agentcode`,
          `cmspdat`.`lacommision`.`wageno`         AS `wageno`,
          `cmspdat`.`lacommision`.`fyc`            AS `fyc`,
          `cmspdat`.`lacommision`.`standprem1`     AS `standprem1`,
          `cmspdat`.`lacommision`.`duymoney`       AS `duymoney`,
          `cmspdat`.`lacommision`.`commdire`       AS `commdire`,
          `cmspdat`.`lacommision`.`payyear`        AS `payyear`,
          `cmspdat`.`lacommision`.`branchtype`     AS `branchtype`,
          `cmspdat`.`lacommision`.`riskchnl`       AS `RISKCHNL`,
          `cmspdat`.`lacommision`.`p6`             AS `p6`,
          `cmspdat`.`lacommision`.`branchcode`     AS `BRANCHCODE`,
          `cmspdat`.`lacommision`.`agentgroup`     AS `agentgroup`,
          `cmspdat`.`lacommision`.`branchseries`   AS `branchseries`,
          `cmspdat`.`lacommision`.`isdirectmarket` AS `isdirectmarket`,
          '1'                                      AS `flag`,
          `cmspdat`.`lacommision`.`baslawcode`     AS `baslawcode`
        from `cmspdat`.`lacommision`
        where ((`cmspdat`.`lacommision`.`branchtype` = '1') and ((`cmspdat`.`lacommision`.`assessvestwage` <> 'Y') or isnull(`cmspdat`.`lacommision`.`assessvestwage`)) and
               ((`cmspdat`.`lacommision`.`isdirectmarket` <> 'Y') or isnull(`cmspdat`.`lacommision`.`isdirectmarket`)) and (`cmspdat`.`lacommision`.`wageno` >= '201705'));

# 优化
# 1- 修改索引
# DROP INDEX idx_laindexinfo1_indexcalno ON laindexinfo1;
# CREATE INDEX idx_laindexinfo1_indexcalno ON laindexinfo1 (indexcalno);

# 2-最终定位到视图（太卡了，加不上索引）

