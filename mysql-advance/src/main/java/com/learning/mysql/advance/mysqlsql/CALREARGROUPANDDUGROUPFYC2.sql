select CALREARGROUPANDDUGROUPFYC2();


"VAGENTCODE: 66030885
VWAGENO: 201709
VASSESSTYPE: 01"

explain SELECT IFNULL(SUM(b.GROUPSTANDPREM * b.BACKRATE), 0)
FROM larearassessinfo b
WHERE rearagentcode = '66030885'
      AND indexcalno = '201709'
      AND rearassesstype = '01'
      AND rearlevel = '01';