select LAAllRelationGroupCardMoney();


VAGENTCODE varchar (10),
VBRANCHCODE varchar (12),
VAGENTGROUP varchar (12),
VWAGENO varchar (8),
VWCSTARTDATE date,
vbaslawcode varchar (20))

"VAGENTCODE: 66030885
VWAGENO: 201709
VASSESSTYPE: 01"
vbaslawcode = BASLAW001


select *
from latree
where agentcode = '66030885';

explain SELECT *
        FROM larearrelation A
        WHERE A.REARLEVEL = '01'
              AND A.REARAGENTCODE = '66030885'
              and A.baslawcode = 'BASLAW001';


explain
select ifnull(
    (select ifnull(standpremmoney, 0)
     from larewardpunish a, latree b
     where doneflag = '12'
           and a.agentcode = b.agentcode
           and b.baslawcode = 'BASLAW001'
           and b.branchcode in
               (SELECT '000000010793'
                FROM DUAL
                UNION ALL
                (SELECT A.AGENTGROUP
                 FROM larearrelation A
                 WHERE A.REARLEVEL = '01'
                       AND A.REARAGENTCODE = '66030885'
                       and A.baslawcode = 'BASLAW001'))
           and a.indexcalno BETWEEN TO_CHAR('2017-05-01', 'YYYYMM') AND '201709'), 0)

from dual;