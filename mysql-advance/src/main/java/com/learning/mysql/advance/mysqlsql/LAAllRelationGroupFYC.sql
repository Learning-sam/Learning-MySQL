select LAAllRelationGroupFYC();

select *
from latree;


SELECT ifnull((SELECT SUM(STANDPREM1)
               FROM LACOMMISION_WT A
               WHERE A.BRANCHTYPE = '1'
                     AND A.PAYYEAR = '0'
                     AND A.COMMDIRE = '1'
                     and A.baslawcode = vbaslawcode
                     AND de_code(VASSESSTYPE, '01', WAGENO1, WAGENO) BETWEEN TO_CHAR(VWCSTARTDATE, 'YYYYMM') AND VWAGENO
                     AND BRANCHCODE IN
                         (SELECT VBRANCHCODE
                          FROM DUAL
                          UNION ALL
                          (SELECT B.AGENTGROUP
                           FROM larearrelation B
                           WHERE B.REARLEVEL = '01'
                                 AND B.REARAGENTCODE = VAGENTCODE
                                 and B.baslawcode = baslawcode))),
              0)
FROM DUAL;

select ifnull((
                select sum(lacommision_wt(null, VWCSTARTDATE, VWAGENO, null, m.branchcode, vbaslawcode, VASSESSTYPE, 'prem'))
                from (SELECT VBRANCHCODE as branchcode
                      FROM DUAL
                      UNION ALL
                      (SELECT B.AGENTGROUP as branchcode
                       FROM larearrelation B
                       WHERE B.REARLEVEL = '01'
                             AND B.REARAGENTCODE = VAGENTCODE
                             and B.baslawcode = baslawcode)) m), 0)
from dual;


select lacommision_wt('66041272', '201804', '201805', NULL, NULL, 'BASLAW001', '00', 'prem');

explain select LAAllRelationGroupFYC('66041272', '000000000146', null, '201805', '2018-04-01', '00', 'BASLAW001');

select sum(lacommision_wt(null, '201804', '201805', null, m.branchcode, 'BASLAW001', '00', 'prem'))
from (SELECT '000000000146' as branchcode
      FROM DUAL
      UNION ALL
      (SELECT B.AGENTGROUP as branchcode
       FROM larearrelation B
       WHERE B.REARLEVEL = '01'
             AND B.REARAGENTCODE = '66041272'
             and B.baslawcode = 'BASLAW001')) m;


SELECT B.AGENTGROUP as branchcode
FROM larearrelation B
WHERE B.REARLEVEL = '01'
      AND B.REARAGENTCODE = '66041272'
/*and B.baslawcode = 'BASLAW001'*/;


drop function if exists LAAllRelationGroupFYC;

create function LAALLRELATIONGROUPFYC(VAGENTCODE varchar(20), VBRANCHCODE varchar(20), VAGENTGROUP varchar(20), VWAGENO varchar(20), VWCSTARTDATE date, VASSESSTYPE varchar(20), vbaslawcode varchar(20))
  returns decimal(16, 4)
  BEGIN
    declare RESULT decimal(16, 4) default 0;
    #直辖组及育成组合计FYC
    /*CS394_9 预警考核 直辖组及育成组合计标保*/
/*SELECT ifnull((SELECT SUM(STANDPREM1)
         FROM lacommision_wt
        WHERE BRANCHTYPE = '1'
          AND PAYYEAR = '0'
          AND COMMDIRE = '1'
          and baslawcode = vbaslawcode
          AND de_code(VASSESSTYPE, '01', WAGENO1, WAGENO) BETWEEN TO_CHAR(VWCSTARTDATE, 'YYYYMM') AND VWAGENO
          AND BRANCHCODE IN
              (SELECT VBRANCHCODE FROM DUAL
                UNION ALL
               (SELECT B.AGENTGROUP
                  FROM larearrelation B
                 WHERE B.REARLEVEL = '01'
                   AND B.REARAGENTCODE = VAGENTCODE
                   and B.baslawcode = baslawcode))),
       0)*/
SELECT ifnull((
                select sum(lacommision_wt(null, TO_CHAR(VWCSTARTDATE, 'YYYYMM'), VWAGENO, null, m.branchcode, vbaslawcode, VASSESSTYPE, 'prem'))
                from (SELECT VBRANCHCODE as branchcode
                      FROM DUAL
                      UNION ALL
                      (SELECT B.AGENTGROUP as branchcode
                       FROM larearrelation B
                       WHERE B.REARLEVEL = '01'
                             AND B.REARAGENTCODE = VAGENTCODE
                             and B.baslawcode = vbaslawcode)) m), 0)
INTO RESULT
FROM DUAL;
set RESULT = ROUND(RESULT, 4);
RETURN (RESULT);
END;


