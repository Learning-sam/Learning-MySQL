select LASumDepartmentCardMoney();


select ifnull(
    (select ifnull(sum(standpremmoney), 0)
     from larewardpunish a, latree b
     where doneflag = '12'
           and a.agentcode = b.agentcode
           and b.baslawcode = ''
           and a.indexcalno between TO_CHAR(VWCSTARTDATE, 'YYYYMM') AND VWAGENO
           AND SUBSTR(b.BRANCHSERIES, 1, 12) = VAGENTGROUP
    ), 0)
from dual;


select
  ifnull((select ifnull(sum(standpremmoney),0)
          from larewardpunish a,latree b
          where doneflag='12'
                and a.agentcode = b.agentcode
                and b.baslawcode = vbaslawcode
                and a.indexcalno between TO_CHAR(VWCSTARTDATE, 'YYYYMM') AND VWAGENO
                AND b.branchcode in (select agentgroup from labranchgroup where branchtype='1' and upbranch = VAGENTGROUP)
         ),0)
from dual;