select LASumGroupADDPerson2();



"VAGENTCODE: 66030885
VWAGENO: 201709
VASSESSTYPE: 01"


explain SELECT IFNULL(sum(1 * a.backrate), 0)
from larearassessinfo  a
WHERE indexcalno = '201709'
      and REARLEVEL = '01'
      and rearagentcode = '66030885'
      and rearassesstype = '01'
      and exists
      (select 1
       from laagent b
       WHERE a.agentcode = b.agentcode
             AND EMPLOYDATE <= LAST_DAY(TO_DATE('201709', 'YYYYMM'))
             AND (OUTWORKDATE IS NULL OR
                  OUTWORKDATE > LAST_DAY(TO_DATE('201709', 'YYYYMM'))))
      AND exists (select 1
                  from latree b
                  where a.rearagentcode = b.INTROAGENCY
                        and b.agentcode = a.agentcode); #排除无推荐关系的