select LASumRelationGroup();


SELECT AGENTSERIES
FROM latree
WHERE AGENTCODE = VAGENTCODE and baslawcode = vbaslawcode;


SELECT UPBRANCH
FROM labranchgroup
WHERE AGENTGROUP = VAGENTGROUP
      and baslawcode = vbaslawcode;


"VAGENTCODE: 66030885
VWAGENO: 201709
VASSESSTYPE: 01"


SELECT ifnull(
    (SELECT COUNT(1)
     FROM larearrelation A
     WHERE A.REARLEVEL = '01'
           AND A.REARAGENTCODE = '66030885'
           AND (A.ENDDATE IS NULL OR A.ENDDATE > LAST_DAY(TO_DATE('201709', 'YYYYMM')))
           AND EXISTS
           (SELECT 1
            FROM latree T, labranchgroup G
            WHERE (T.INSIDEFLAG <> '1' OR T.INSIDEFLAG IS NULL)
                  AND T.AGENTCODE = A.AGENTCODE
                  AND G.AGENTGROUP = T.BRANCHCODE
           )), 0)
FROM DUAL;

